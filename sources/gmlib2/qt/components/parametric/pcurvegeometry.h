#ifndef GM2QT_PCURVEGEOMETRY_H
#define GM2QT_PCURVEGEOMETRY_H


#include "../../sceneobject.h"


// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DRender>

// stl
#include <memory>


namespace gmlib2::qt::parametric
{

  template <typename PCurveBase_T>
  class PCurve;

  namespace detail
  {

    class PCurveWrapperInterface {
    public:
      PCurveWrapperInterface()          = default;
      virtual ~PCurveWrapperInterface() = default;

      using EvaluationResult = DVectorT<VectorT<double, 4>>;
      using SampleResult     = DVectorT<EvaluationResult>;

      virtual SampleResult sample(size_t samples, size_t derivatives) const = 0;
    };

    template <typename PCurveBase_T>
    class PCurveWrapper final : public PCurveWrapperInterface {
    public:
      using PCurve = gmlib2::qt::parametric::PCurve<PCurveBase_T>;

      PCurveWrapper(PCurve* curve) : m_curve{curve}
      {
        static_assert(std::is_same<PCurveWrapperInterface::SampleResult,
                                   typename PCurveBase_T::SampleResult>::value,
                      "PCurve does not have expected SampleResult type!");
      }
      ~PCurveWrapper() override = default;


      // PolygonSurfaceWrapper interface
    public:
      SampleResult sample(size_t samples, size_t derivatives) const override
      {
        if (m_curve)
          return m_curve->sample(PSizeArray{{samples}},
                                 PSizeArray{{derivatives}});
        else
          return SampleResult();
      }


    private:
      using PSizeArray = typename PCurve::PSpace_SizeArray_Type;
      PCurve* m_curve{nullptr};
    };
  }   // namespace detail










  class PCurveGeometry : public Qt3DRender::QGeometry {
    Q_OBJECT
  public:
    template <typename PCurveBase_T>
    PCurveGeometry(gmlib2::qt::parametric::PCurve<PCurveBase_T>* pcurve,
                   QNode* parent = nullptr);
    ~PCurveGeometry() override = default;


    int   samples() const;
    int   slices() const;
    float radius() const;

  public slots:
    void reSample();
    void setSamples(int rings);
    void setSlices(int slices);
    void setRadius(float radius);

  signals:
    void samplesChanged(int);
    void slicesChanged(int);
    void radiusChanged(float);

  private:
    struct VertexElement {
      std::array<float, 3> p;
      std::array<float, 3> n;
      std::array<float, 4> t;

      void printInline() const
      {
        std::cout << "p: " << p[0] << ", " << p[1] << ", " << p[2];
        std::cout << ", n: " << n[0] << ", " << n[1] << ", " << n[2];
        std::cout << ", t: " << t[0] << ", " << t[1] << ", " << t[2] << ", "
                  << t[3];
      }
      void print() const
      {
        printInline();
        std::cout << std::endl;
      }
    };

    struct IndexElement {
      std::array<quint32, 3> i;

      void print() const
      {
        std::cout << i[0] << ", " << i[1] << ", " << i[2] << std::endl;
        ;
      }
    };

    void init();
    void updateVertices();
    void updateIndices();
    void sample();

    int vertexCount() const;
    int faceCount() const;


    // Default properties
    int   m_samples{20};
    int   m_slices{10};
    float m_radius{0.1f};

    std::unique_ptr<detail::PCurveWrapperInterface> m_internal_curve{nullptr};
    Qt3DRender::QBuffer*                            m_vertex_buffer;
    Qt3DRender::QBuffer*                            m_index_buffer;
    Qt3DRender::QAttribute*                         m_position_attribute;
    Qt3DRender::QAttribute*                         m_normal_attribute;
    Qt3DRender::QAttribute*                         m_index_attribute;
    Qt3DRender::QAttribute*                         m_tangent_attribute;
  };



  template <typename PCurveBase_T>
  PCurveGeometry::PCurveGeometry(
    gmlib2::qt::parametric::PCurve<PCurveBase_T>* pcurve, QNode* parent)
    : Qt3DRender::QGeometry(parent),

      m_internal_curve{
        std::make_unique<detail::PCurveWrapper<PCurveBase_T>>(pcurve)}
  {
    init();
    updateVertices();
    updateIndices();
  }










  class PCurveMesh : public Qt3DRender::QGeometryRenderer {
    Q_OBJECT

    Q_PROPERTY(int samples READ samples WRITE setSamples NOTIFY samplesChanged)
    Q_PROPERTY(int slices READ slices WRITE setSlices NOTIFY slicesChanged)
    Q_PROPERTY(float radius READ radius WRITE setRadius NOTIFY radiusChanged)

  public:
    template <typename PCurveBase_T>
    PCurveMesh(gmlib2::qt::parametric::PCurve<PCurveBase_T>* pcurve);

    int   samples() const;
    int   slices() const;
    float radius() const;

  public slots:
    void reSample();
    void setSamples(int rings = 20);
    void setSlices(int slices = 10);
    void setRadius(float radius = 0.1f);

  signals:
    void samplesChanged(int);
    void slicesChanged(int);
    void radiusChanged(float);

  private:
    PCurveGeometry* m_geometry{nullptr};
    bool            m_geometry_initialized{false};
  };



  template <typename PCurveBase_T>
  PCurveMesh::PCurveMesh(gmlib2::qt::parametric::PCurve<PCurveBase_T>* pcurve)
    : Qt3DRender::QGeometryRenderer(pcurve)
  {
    static_assert(std::is_base_of<SceneObject, PCurveBase_T>::value, "...");

    m_geometry = new PCurveGeometry(pcurve, this);
    connect(m_geometry, &PCurveGeometry::samplesChanged, this,
            &PCurveMesh::samplesChanged);
    connect(m_geometry, &PCurveGeometry::slicesChanged, this,
            &PCurveMesh::slicesChanged);
    connect(m_geometry, &PCurveGeometry::radiusChanged, this,
            &PCurveMesh::radiusChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }

}   // namespace gmlib2::qt::parametric


#endif   // GM2QT_PCURVEGEOMETRY_H
