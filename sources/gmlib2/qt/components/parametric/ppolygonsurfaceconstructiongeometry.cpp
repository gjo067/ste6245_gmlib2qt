#include "ppolygonsurfaceconstructiongeometry.h"


// gmlib2
#include <gmlib2.h>

// stl
#include <functional>
#include <numeric>

namespace gmlib2::qt::parametric
{

  int PPolygonSurfaceConstructionGeometry::samples() const { return m_samples; }

  void PPolygonSurfaceConstructionGeometry::setSamples(int samples)
  {
    if(samples < 2)  return;

    if (m_samples not_eq samples) {
      m_samples = samples;
      updateVertices();
      updateIndices();
      emit samplesChanged(m_samples);
    }
  }

  void PPolygonSurfaceConstructionGeometry::updateVertices()
  {

    assert(m_samples > 1);

//    qDebug() << "Generating geometry! BEGIN";
    auto* polygon = m_internal_polygon.get();

    std::cout << "  Polygon dims [sides]: " << polygon->sides() << std::endl;
    auto sample_set = polygon->sample(size_t(m_samples));   // size_t(resolution));
        std::cout << "  PolygonSurfaceConstruction sample set:\n" << sample_set << std::endl;

//    const auto vert_samp_no = sample_set.size() - 1;




    //    const size_t no_tris  = polygon->sides();
    const auto no_verts = vertexCount();

    m_position_attribute->setCount(quint32(no_verts));
    m_normal_attribute->setCount(quint32(no_verts));
    m_tangent_attribute->setCount(quint32(no_verts));

    QByteArray buffer_bytes;
    buffer_bytes.resize(no_verts * int(sizeof(VertexElement)));
    VertexElement* ptr = reinterpret_cast<VertexElement*>(buffer_bytes.data());


    const int N = int(m_internal_polygon->sides());
    const int M = m_samples;


    for( int m = 0; m < M; ++m ) {

      int T_prev = m-2;
      int T = m-1;

      int r_o_prev;
      if( m-1 == 0 ) r_o_prev = 0;
      else if( m-1 == 1 ) r_o_prev = 1;
      else {
        r_o_prev = 1;
        for( int k = 2; k <= m-1; ++k ) {
          r_o_prev += N * (k-1);
        }
      }

      int r_o;
      if( m == 0 ) r_o = 0;
      else if( m == 1 ) r_o = 1;
      else {
        r_o = 1;
        for( int k = 2; k <= m; ++k ) {
          r_o += N * (k-1);
        }
      }

      for( int n = 0; n < (m==0?1:N); ++n) {

        int t_o_prev = r_o_prev + n * T_prev + n;
        int t_o = r_o + n * T + n;



        for( int t_i = 0; t_i <= T; ++t_i ) {

          const int idx = t_o + t_i;
//          std::cout << "  n,m: " << n << "," << m << std::endl;
//          std::cout << "     -> r_o: " << r_o
//                    << ", t_o: " << t_o << ", idx: " << idx << std::endl;

//          double u = 1.0 - ((1.0 / double(M-1)) * m);
//          double w = (1.0 / double(M-1)) * t_i;
//          double v = 1.0 - (u+w);

//          std::cout << "     -> u,v,w: "
//                    << "(" << u << "," << v << "," << w << ")"
//                    << std::endl;

//          auto poly_par = center * u + par[n] * v + par[n+1] * w;


//          std::cout << "     -> poly_par: " << std::endl << poly_par << std::endl;


          const auto p0 = sample_set[size_t(idx)][0];
          ptr[idx].p[0] = float(p0[0]);
          ptr[idx].p[1] = float(p0[1]);
          ptr[idx].p[2] = float(p0[2]);

          if(m>0) {

            if(sample_set[size_t(idx)].size() > 1) {

              const auto du = blaze::normalize(
                blaze::subvector(sample_set[size_t(idx)][1], 0UL, 3UL));
              const auto dv = blaze::normalize(
                blaze::subvector(sample_set[size_t(idx)][2], 0UL, 3UL));
              const auto norm = blaze::normalize(blaze::cross(du, dv));
              ptr[idx].n[0] = float(norm[0]);
              ptr[idx].n[1] = float(norm[1]);
              ptr[idx].n[2] = float(norm[2]);
            }
            else {

              if(n == N-1 and t_i == T) {
                const int idx_p1 = r_o_prev;
                const int idx_p2 = r_o;

                const auto p1 = sample_set[size_t(idx_p1)][0];
                const auto p2 = sample_set[size_t(idx_p2)][0];

                const auto du = blaze::normalize(blaze::subvector(p1 - p0, 0UL, 3UL));
                const auto dv = blaze::normalize(blaze::subvector(p2 - p0, 0UL, 3UL));
                const auto norm  = blaze::normalize(blaze::cross(du, dv));
                ptr[idx].n[0] = float(norm[0]);
                ptr[idx].n[1] = float(norm[1]);
                ptr[idx].n[2] = float(norm[2]);

              }
              else {
                const int idx_p1 = t_o_prev + t_i;
                const int idx_p2 = t_o + t_i + 1;

                const auto p1 = sample_set[size_t(idx_p1)][0];
                const auto p2 = sample_set[size_t(idx_p2)][0];

                const auto du = blaze::normalize(blaze::subvector(p1 - p0, 0UL, 3UL));
                const auto dv = blaze::normalize(blaze::subvector(p2 - p0, 0UL, 3UL));
                const auto norm  = blaze::normalize(blaze::cross(du, dv));
                ptr[idx].n[0] = float(norm[0]);
                ptr[idx].n[1] = float(norm[1]);
                ptr[idx].n[2] = float(norm[2]);
              }
            }
          }
        }
      }
    }


    const auto c_p0 = sample_set[0UL][0];
    ptr[0].p[0] = float(c_p0[0]);
    ptr[0].p[1] = float(c_p0[1]);
    ptr[0].p[2] = float(c_p0[2]);

    if(sample_set[0UL].size() > 1) {

      const auto du = blaze::normalize(
        blaze::subvector(sample_set[0UL][1], 0UL, 3UL));
      const auto dv = blaze::normalize(
        blaze::subvector(sample_set[0UL][2], 0UL, 3UL));
      const auto c_norm = blaze::normalize(blaze::cross(du, dv));
      ptr[0].n[0] = float(c_norm[0]);
      ptr[0].n[1] = float(c_norm[1]);
      ptr[0].n[2] = float(c_norm[2]);
    }
    else {

      gmlib2::VectorT<double, 3UL> c_n(0.0);
      for( int i = 0; i < N; ++i ) {
        const auto& a = ptr[i+1];
        c_n += gmlib2::VectorT<double, 3UL>{ double(a.n[0]), double(a.n[1]), double(a.n[2]) };
      }

      auto c_norm = blaze::normalize(c_n);
      ptr[0].n[0] = float(c_norm[0]);
      ptr[0].n[1] = float(c_norm[1]);
      ptr[0].n[2] = float(c_norm[2]);
    }


//    for( int i = 0; i < no_verts; ++i ) {
//      std::cout << "i: " << i << "; p: (" << ptr[i].p[0] << "," << ptr[i].p[1]
//                << "," << ptr[i].p[2] << ")" << std::endl;
//    }

    m_vertex_buffer->setData(buffer_bytes);
  }

  void PPolygonSurfaceConstructionGeometry::updateIndices()
  {




//    auto indexElement = []( const auto& p0, const auto& p1, const auto& p2 ) {
//      return IndexElement{quint32(p0),quint32(p1),quint32(p2)};
//    };




    const auto no_faces = faceCount();

    m_index_attribute->setCount(quint32(no_faces * 3));

    const auto no_indices = no_faces;
    QByteArray   indexBytes;
    indexBytes.resize(no_indices*int(sizeof(IndexElement)));
    IndexElement* iptr = reinterpret_cast<IndexElement*>(indexBytes.data());




    const int N = int(m_internal_polygon->sides());
    const int M = m_samples;



//    std::cout << "M,N: " << M << ',' << N << std::endl;
//    std::cout << "No. Vertices: " << vertexCount() << std::endl;
//    std::cout << "No. Faces: " << no_faces << std::endl;
//    std::cout << "No. Indices: " << no_indices << std::endl;

//    // Debug -- init
//    for( int i = 0; i < no_indices; ++i ) iptr[i].i = {0,0,0};
//    // Debug -- init -- END



    for( int m = 0; m <= M-1; ++m ) {

      int T_prev = m-2;
      int T = m-1;

      int r_o_prev;
      if( m-1 == 0 ) r_o_prev = 0;
      else if( m-1 == 1 ) r_o_prev = 1;
      else {
        r_o_prev = 1;
        for( int k = 2; k <= m-1; ++k ) {
          r_o_prev += N * (k-1);
        }
      }

      int r_o;
      if( m == 0 ) r_o = 0;
      else if( m == 1 ) r_o = 1;
      else {
        r_o = 1;
        for( int k = 2; k <= m; ++k ) {
          r_o += N * (k-1);
        }
      }

      int f_T = 2 * (m-1);

      int f_o = 0;
      if(m>0) {
        for( int k = 0; k <= m-2; ++k )
          f_o += N * (2 * (k) + 1);
      }


      for( int n = 0; n < (m==0?1:N); ++n) {

        int t_o_prev = r_o_prev + n * T_prev + n;
        int t_o = r_o + n * T + n;

        int f_t_o = f_o + n * f_T + n;


        for( int t_i = 0; t_i <= T; ++t_i ) {

//          const int idx = t_o + t_i;
          const int f_t_i = f_t_o + t_i * 2;


          if( t_i == T and n == N-1) {

            const int i0_0 = r_o_prev;
            const int i0_1 = t_o + t_i;
            const int i0_2 = r_o;

//            std::cout << "  n,m: " << n << "," << m;
//            std::cout << "  -> r_o: " << r_o << ", t_o: " << t_o
//                      << ", f_o: " << f_o << ", f_t_o: " <<f_t_o
//                      << ", f_t_i: " << f_t_i << ", idx: " << idx
//                      << ", i_0[]: (" << i0_0 << "," << i0_1 << "," << i0_2 <<")"
//                      << std::endl;
            iptr[f_t_i] = {quint32(i0_0),quint32(i0_1),quint32(i0_2)};
          }
          else if( t_i == T ) {

            const int i0_0 = t_o_prev + t_i;
            const int i0_1 = t_o + t_i;
            const int i0_2 = t_o + t_i + 1;

//            std::cout << "  n,m: " << n << "," << m;
//            std::cout << "  -> r_o: " << r_o << ", t_o: " << t_o
//                      << ", f_o: " << f_o << ", f_t_o: " <<f_t_o
//                      << ", f_t_i: " << f_t_i << ", idx: " << idx
//                      << ", i_0[]: (" << i0_0 << "," << i0_1 << "," << i0_2 <<")"
//                      << std::endl;
            iptr[f_t_i] = {quint32(i0_0),quint32(i0_1),quint32(i0_2)};
          }
          else {

            const int i0_0 = t_o_prev + t_i;
            const int i0_1 = t_o + t_i;
            const int i0_2 = t_o + t_i + 1;

            const int i1_0 = t_o_prev + t_i;
            const int i1_1 = t_o + t_i + 1;
            const int i1_2 = t_o_prev + t_i + 1;

            iptr[f_t_i] = {quint32(i0_0),quint32(i0_1),quint32(i0_2)};
            if(n == N-1 and t_i == T-1) {
              iptr[f_t_i+1] = {quint32(i1_0),quint32(i1_1),quint32(r_o_prev)};
//              std::cout << "  n,m: " << n << "," << m;
//              std::cout << "  -> r_o: " << r_o << ", t_o: " << t_o
//                        << ", f_o: " << f_o << ", f_t_o: " <<f_t_o
//                        << ", f_t_i: " << f_t_i << ", idx: " << idx
//                        << ", i_0[]: (" << i0_0 << "," << i0_1 << "," << i0_2 <<")"
//                        << ", i_1[]: (" << i1_0 << "," << i1_1 << "," << r_o_prev <<")"
//                        << std::endl;
            }
            else {

              iptr[f_t_i+1] = {quint32(i1_0),quint32(i1_1),quint32(i1_2)};

//              std::cout << "  n,m: " << n << "," << m;
//              std::cout << "  -> r_o: " << r_o << ", t_o: " << t_o
//                        << ", f_o: " << f_o << ", f_t_o: " <<f_t_o
//                        << ", f_t_i: " << f_t_i << ", idx: " << idx
//                        << ", i_0[]: (" << i0_0 << "," << i0_1 << "," << i0_2 <<")"
//                        << ", i_1[]: (" << i1_0 << "," << i1_1 << "," << i1_2 <<")"
//                        << std::endl;
            }

            //          double u = 1.0 - ((1.0 / double(M-1)) * m);
            //          double w = (1.0 / double(M-1)) * t_i;
            //          double v = 1.0 - (u+w);

            //          std::cout << "     -> u,v,w: "
            //                    << "(" << u << "," << v << "," << w << ")"
            //                    << std::endl;

            //          auto poly_par = center * u + par[n] * v + par[n+1] * w;


            //          std::cout << "     -> poly_par: " << std::endl << poly_par
            //          << std::endl;


            //          auto p0 = sample_set[size_t(idx)][0];
            //          ptr[idx].p[0] = float(p0[0]);
            //          ptr[idx].p[1] = float(p0[1]);
            //          ptr[idx].p[2] = float(p0[2]);

          }

        }
      }
    }





//    for (int i = 0; i < no_indices; ++i) {
//      std::cout << "i: " << i << "; i: (" << iptr[i].i[0] << "," << iptr[i].i[1]
//                << "," << iptr[i].i[2] << ")" << std::endl;
//    }

    m_index_buffer->setData(indexBytes);
  }

  void PPolygonSurfaceConstructionGeometry::init()
  {
    m_vertex_buffer
      = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, this);
    m_index_buffer
      = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::IndexBuffer, this);

    const int stride = sizeof(VertexElement);

    const int no_vertices = vertexCount();
    const int no_faces    = faceCount();

    m_position_attribute = new Qt3DRender::QAttribute(this);
    m_position_attribute->setName(
      Qt3DRender::QAttribute::defaultPositionAttributeName());
    m_position_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_position_attribute->setVertexSize(3);
    m_position_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_position_attribute->setBuffer(m_vertex_buffer);
    m_position_attribute->setByteStride(stride);
    m_position_attribute->setCount(quint32(no_vertices));

    m_normal_attribute = new Qt3DRender::QAttribute(this);
    m_normal_attribute->setName(
      Qt3DRender::QAttribute::defaultNormalAttributeName());
    m_normal_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_normal_attribute->setVertexSize(3);
    m_normal_attribute->setByteOffset(3 * sizeof(float));
    m_normal_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_normal_attribute->setBuffer(m_vertex_buffer);
    m_normal_attribute->setByteStride(stride);
    m_normal_attribute->setCount(quint32(no_vertices));


    m_tangent_attribute = new Qt3DRender::QAttribute(this);
    m_tangent_attribute->setName(
      Qt3DRender::QAttribute::defaultTangentAttributeName());
    m_tangent_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_tangent_attribute->setVertexSize(4);
    m_tangent_attribute->setByteOffset(6 * sizeof(float));
    m_tangent_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_tangent_attribute->setBuffer(m_vertex_buffer);
    m_tangent_attribute->setByteStride(stride);
    m_tangent_attribute->setCount(quint32(no_vertices));

    m_index_attribute = new Qt3DRender::QAttribute(this);
    m_index_attribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    m_index_attribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    m_index_attribute->setBuffer(m_index_buffer);
    m_index_attribute->setCount(quint32(no_faces * 3));


    addAttribute(m_position_attribute);
    addAttribute(m_normal_attribute);
    addAttribute(m_tangent_attribute);
    addAttribute(m_index_attribute);
  }

  int PPolygonSurfaceConstructionGeometry::vertexCount() const
  {
    assert(m_samples > 1);

    int no_vertices = 1;
    for( int k = 2; k <= m_samples; ++k )
      no_vertices += int(m_internal_polygon->sides()) * (k-1);

    return no_vertices;
  }

  int PPolygonSurfaceConstructionGeometry::faceCount() const
  {
    assert(m_samples > 1);

    if(m_samples == 2) return int(m_internal_polygon->sides());

    int no_faces = 1;
    for( int k = 3; k <= m_samples; ++k )
      no_faces += 1 + (k - 2) * 2;
    no_faces *= int(m_internal_polygon->sides());

    return no_faces ;
  }



  int PPolygonSurfaceConstructionMesh::samples() const { return m_geometry->samples(); }

  void PPolygonSurfaceConstructionMesh::setSamples(int samples)
  {
    m_geometry->setSamples(samples);
  }

}   // namespace gmlib2::qt::parametric
