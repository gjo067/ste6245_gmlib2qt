#ifndef GM2QT_PPOLYGONSURFACECONSTRUCTIONGEOMETRY_H
#define GM2QT_PPOLYGONSURFACECONSTRUCTIONGEOMETRY_H


#include "../../sceneobject.h"

// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DExtras>

namespace gmlib2::qt::parametric
{

  // Forward declare
  template <typename PPolygonSurfaceConstruction_Type>
  class PPolygonSurfaceConstruction;



  namespace detail
  {

    class PPolygonSurfaceConstructionWrapperInterface {
    public:
      PPolygonSurfaceConstructionWrapperInterface()          = default;
      virtual ~PPolygonSurfaceConstructionWrapperInterface() = default;

      using EvaluationResult = DVectorT<VectorT<double, 4>>;
      using SampleResult     = DVectorT<EvaluationResult>;

      virtual size_t       sides() const                  = 0;
      virtual SampleResult sample(size_t samples_M) const = 0;
    };

    template <typename PPolygonSurfaceConstructionBase_T>
    class PPolygonSurfaceConstructionWrapper final
      : public PPolygonSurfaceConstructionWrapperInterface {
    public:
      using PPolygonSurfaceConstruction
        = gmlib2::qt::parametric::PPolygonSurfaceConstruction<
          PPolygonSurfaceConstructionBase_T>;

      PPolygonSurfaceConstructionWrapper(PPolygonSurfaceConstruction* surface)
        : m_polygonsurface{surface}
      {
        static_assert(
          std::is_same<
            PPolygonSurfaceConstructionWrapperInterface::SampleResult,
            typename PPolygonSurfaceConstructionBase_T::SampleResult>::value,
          "PPolygonSurfaceConstruction does not have expected SampleResult "
          "type!");
      }
      ~PPolygonSurfaceConstructionWrapper() override = default;


      // PPolygonSurfaceConstructionWrapperInterface interface
    public:
      size_t sides() const override
      {
        if (m_polygonsurface)
          return m_polygonsurface->sides();
        else
          return 0;
      }

      SampleResult sample(size_t samples_M) const override
      {
        if (m_polygonsurface)
          return m_polygonsurface->sample(samples_M);
        else
          return SampleResult();
      }


    private:
      using PSizeArray =
        typename PPolygonSurfaceConstruction::PSpace_SizeArray_Type;
      PPolygonSurfaceConstruction* m_polygonsurface{nullptr};
    };
  }   // namespace detail










  class PPolygonSurfaceConstructionGeometry : public Qt3DRender::QGeometry {
    Q_OBJECT
  public:
    template <typename PPolygonSurfaceConstructionBase_T>
    PPolygonSurfaceConstructionGeometry(
      gmlib2::qt::parametric::PPolygonSurfaceConstruction<
        PPolygonSurfaceConstructionBase_T>* surface,
      QNode*                                parent);
    ~PPolygonSurfaceConstructionGeometry() override = default;


    int samples() const;

  public slots:
    void setSamples(int samples = 3);

  signals:
    void samplesChanged(int samples);


  private:
    struct VertexElement {
      std::array<float, 3> p;
      std::array<float, 3> n;
      std::array<float, 4> t;


      void printInline() const
      {
        std::cout << "p: " << p[0] << ", " << p[1] << ", " << p[2];
        std::cout << ", n: " << n[0] << ", " << n[1] << ", " << n[2];
        std::cout << ", t: " << t[0] << ", " << t[1] << ", " << t[2] << ", "
                  << t[3];
      }

      void print() const
      {
        printInline();
        std::cout << std::endl;
      }
    };

    struct IndexElement {
      std::array<quint32, 3> i;

      void print() const
      {
        std::cout << i[0] << ", " << i[1] << ", " << i[2] << std::endl;
        ;
      }
    };



    void init();
    void updateVertices();
    void updateIndices();

    int vertexCount() const;
    int faceCount() const;



    // Default properties
    int m_samples{3};

    std::unique_ptr<detail::PPolygonSurfaceConstructionWrapperInterface>
                            m_internal_polygon{nullptr};
    Qt3DRender::QBuffer*    m_vertex_buffer;
    Qt3DRender::QBuffer*    m_index_buffer;
    Qt3DRender::QAttribute* m_position_attribute;
    Qt3DRender::QAttribute* m_normal_attribute;
    Qt3DRender::QAttribute* m_index_attribute;
    Qt3DRender::QAttribute* m_tangent_attribute;
  };



  template <typename PPolygonSurfaceConstructionBase_T>
  PPolygonSurfaceConstructionGeometry::PPolygonSurfaceConstructionGeometry(
    gmlib2::qt::parametric::PPolygonSurfaceConstruction<
      PPolygonSurfaceConstructionBase_T>* surface,
    QNode*                                parent)
    : Qt3DRender::QGeometry(parent),
      m_internal_polygon{
        std::make_unique<detail::PPolygonSurfaceConstructionWrapper<
          PPolygonSurfaceConstructionBase_T>>(surface)}
  {
    init();
    updateVertices();
    updateIndices();
  }










  class PPolygonSurfaceConstructionMesh : public Qt3DRender::QGeometryRenderer {
    Q_OBJECT

  public:
    template <typename PPolygonSurfaceConstructionBase_T>
    PPolygonSurfaceConstructionMesh(
      gmlib2::qt::parametric::PPolygonSurfaceConstruction<
        PPolygonSurfaceConstructionBase_T>* surface);

    int samples() const;

  public slots:
    void setSamples(int samples = 3);

  signals:
    void samplesChanged(int samples);

  private:
    PPolygonSurfaceConstructionGeometry* m_geometry;
    bool                                 m_geometry_initialized{false};
  };



  template <typename PPolygonSurfaceConstructionBase_T>
  PPolygonSurfaceConstructionMesh::PPolygonSurfaceConstructionMesh(
    gmlib2::qt::parametric::PPolygonSurfaceConstruction<
      PPolygonSurfaceConstructionBase_T>* surface)
    : Qt3DRender::QGeometryRenderer(surface)
  {
    static_assert(
      std::is_base_of<SceneObject, PPolygonSurfaceConstructionBase_T>::value,
      "...");
    m_geometry = new PPolygonSurfaceConstructionGeometry(surface, this);
    connect(m_geometry, &PPolygonSurfaceConstructionGeometry::samplesChanged,
            this, &PPolygonSurfaceConstructionMesh::samplesChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }


}   // namespace gmlib2::qt::parametric

#endif   // GM2QT_PPOLYGONSURFACECONSTRUCTIONGEOMETRY_H
