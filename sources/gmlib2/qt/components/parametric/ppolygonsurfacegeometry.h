#ifndef GM2QT_PPOLYGONSURFACEGEOMETRY_H
#define GM2QT_PPOLYGONSURFACEGEOMETRY_H


#include "../../sceneobject.h"

// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DExtras>

namespace gmlib2::qt::parametric
{

  // Forward declare
  template <typename PPolygonSurface_Type>
  class PPolygonSurface;



  namespace detail
  {

    class PPolygonSurfaceWrapperInterface {
    public:
      PPolygonSurfaceWrapperInterface()          = default;
      virtual ~PPolygonSurfaceWrapperInterface() = default;

      using EvaluationResult = DVectorT<VectorT<double, 4>>;
      using SampleResult     = DVectorT<EvaluationResult>;

      virtual size_t       sides() const  = 0;
      virtual SampleResult sample(size_t samples_M) const = 0;
    };

    template <typename PPolygonSurfaceBase_T>
    class PPolygonSurfaceWrapper final
      : public PPolygonSurfaceWrapperInterface {
    public:
      using PPolygonSurface = gmlib2::qt::parametric::PPolygonSurface<PPolygonSurfaceBase_T>;

      PPolygonSurfaceWrapper(PPolygonSurface* surface)
        : m_polygonsurface{surface}
      {
        static_assert(
          std::is_same<PPolygonSurfaceWrapperInterface::SampleResult,
                       typename PPolygonSurfaceBase_T::SampleResult>::value,
          "PPolygonSurface does not have expected SampleResult type!");
      }
      ~PPolygonSurfaceWrapper() override = default;


      // PPolygonSurfaceWrapperInterface interface
    public:
      size_t sides() const override
      {
        if (m_polygonsurface)
          return m_polygonsurface->PSpace_VectorDim;
        else
          return 0;
      }

      SampleResult sample(size_t samples_M) const override
      {
        if (m_polygonsurface)
          return m_polygonsurface->sample(samples_M);
        else
          return SampleResult();
      }


    private:
      using PSizeArray = typename PPolygonSurface::PSpace_SizeArray_Type;
      PPolygonSurface* m_polygonsurface{nullptr};
    };
  }   // namespace detail










  class PPolygonSurfaceGeometry : public Qt3DRender::QGeometry {
    Q_OBJECT
  public:
    template <typename PPolygonSurfaceBase_T>
    PPolygonSurfaceGeometry(
      gmlib2::qt::parametric::PPolygonSurface<PPolygonSurfaceBase_T>* surface,
      QNode*                                                          parent);
    ~PPolygonSurfaceGeometry() override = default;


    int samples() const;

  public slots:
    void reSample();
    void setSamples(int samples = 3);

  signals:
    void samplesChanged(int samples);


  private:
    struct VertexElement {
      std::array<float, 3> p;
      std::array<float, 3> n;
      std::array<float, 4> t;


      void printInline() const
      {
        std::cout << "p: " << p[0] << ", " << p[1] << ", " << p[2];
        std::cout << ", n: " << n[0] << ", " << n[1] << ", " << n[2];
        std::cout << ", t: " << t[0] << ", " << t[1] << ", " << t[2] << ", "
                  << t[3];
      }

      void print() const
      {
        printInline();
        std::cout << std::endl;
      }
    };

    struct IndexElement {
      std::array<quint32, 3> i;

      void print() const
      {
        std::cout << i[0] << ", " << i[1] << ", " << i[2] << std::endl;
        ;
      }
    };



    void init();
    void updateVertices();
    void updateIndices();

    int vertexCount() const;
    int faceCount() const;

    void sample();



    // Default properties
    int m_samples{3};

    std::unique_ptr<detail::PPolygonSurfaceWrapperInterface> m_internal_polygon{
      nullptr};
    Qt3DRender::QBuffer*    m_vertex_buffer;
    Qt3DRender::QBuffer*    m_index_buffer;
    Qt3DRender::QAttribute* m_position_attribute;
    Qt3DRender::QAttribute* m_normal_attribute;
    Qt3DRender::QAttribute* m_index_attribute;
    Qt3DRender::QAttribute* m_tangent_attribute;
  };



  template <typename PPolygonSurfaceBase_T>
  PPolygonSurfaceGeometry::PPolygonSurfaceGeometry(
    gmlib2::qt::parametric::PPolygonSurface<PPolygonSurfaceBase_T>* surface,
    QNode*                                                          parent)
    : Qt3DRender::QGeometry(parent),
      m_internal_polygon{
        std::make_unique<detail::PPolygonSurfaceWrapper<PPolygonSurfaceBase_T>>(
          surface)}
  {
    init();
    updateVertices();
    updateIndices();
  }










  class PPolygonSurfaceMesh : public Qt3DRender::QGeometryRenderer {
    Q_OBJECT

  public:
    template <typename PPolygonSurfaceBase_T>
    PPolygonSurfaceMesh(
      gmlib2::qt::parametric::PPolygonSurface<PPolygonSurfaceBase_T>* surface);

    int samples() const;

  public slots:
    void reSample();
    void setSamples(int samples = 3);

  signals:
    void samplesChanged(int samples);

  private:
    PPolygonSurfaceGeometry* m_geometry;
    bool                     m_geometry_initialized{false};
  };



  template <typename PPolygonSurfaceBase_T>
  PPolygonSurfaceMesh::PPolygonSurfaceMesh(
    gmlib2::qt::parametric::PPolygonSurface<PPolygonSurfaceBase_T>* surface)
    : Qt3DRender::QGeometryRenderer(surface)
  {
    static_assert(std::is_base_of<SceneObject, PPolygonSurfaceBase_T>::value,
                  "...");
    m_geometry = new PPolygonSurfaceGeometry(surface, this);
    connect(m_geometry, &PPolygonSurfaceGeometry::samplesChanged, this,
            &PPolygonSurfaceMesh::samplesChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }


}   // namespace gmlib2::qt::parametric

#endif   // GM2QT_PPOLYGONSURFACEGEOMETRY_H
