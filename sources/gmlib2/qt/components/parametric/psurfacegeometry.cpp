#include "psurfacegeometry.h"

// stl
#include <iostream>
#include <iomanip>


namespace gmlib2::qt::parametric
{


  void PSurfaceGeometry::updateVertices()
  {
    auto* surface = m_internal_surface.get();

    auto sample_set = surface->sample(m_samples, QSize(1, 1));

//    std::cout << "Sample set:" << std::endl;
//    std::cout << std::setfill(' ') << std::setw(4) << ' ';
//    std::cout << "";
//    for (size_t j = 0; j < sample_set.columns(); ++j) {
//      std::cout << std::setfill(' ') << std::setw(20) << j;
//    }
//    std::cout << std::endl;

//    for (size_t i = 0; i < sample_set.rows(); ++i) {

//      std::cout << std::left << std::setfill(' ') << std::setw(4) << i;
//      for (size_t j = 0; j < sample_set.columns(); ++j) {
//        auto d = sample_set(i,j)(0,0);
//        std::cout << " [";
//        std::cout << std::fixed << std::setprecision(1) << std::setfill(' ') << std::setw(5) << std::right << d[0];
//        std::cout << ",";
//        std::cout << std::fixed << std::setprecision(1) << std::setfill(' ') << std::setw(5) << std::right << d[1];
//        std::cout << ",";
//        std::cout << std::fixed << std::setprecision(1) << std::setfill(' ') << std::setw(5) << std::right << d[2];
//        std::cout << "]";
//      }
//      std::cout << std::endl;
//    }




    const int stride = sizeof(VertexElement);

    const int no_verts = vertexCount();

    m_position_attribute->setCount(quint32(no_verts));
    m_normal_attribute->setCount(quint32(no_verts));
    m_tangent_attribute->setCount(quint32(no_verts));


    QByteArray buffer_bytes;
    buffer_bytes.resize(int(stride * no_verts));
    VertexElement* ptr = reinterpret_cast<VertexElement*>(buffer_bytes.data());

    for (int i = 0; i < m_samples.width(); ++i) {
      for (int j = 0; j < m_samples.height(); ++j) {

        // Extract data
        const auto& func   = blaze::eval((sample_set(size_t(i), size_t(j)))(0, 0));
        const auto& der_u  = blaze::eval((sample_set(size_t(i), size_t(j)))(1, 0));
        const auto& der_v  = blaze::eval((sample_set(size_t(i), size_t(j)))(0, 1));
        const auto  normal = blaze::cross(blaze::subvector(der_u, 0UL, 3UL),
                                         blaze::subvector(der_v, 0UL, 3UL));

        // Position
        ptr->p[0] = float(func[0]);
        ptr->p[1] = float(func[1]);
        ptr->p[2] = float(func[2]);

        // Normal
        ptr->n[0] = float(normal[0]);
        ptr->n[1] = float(normal[1]);
        ptr->n[2] = float(normal[2]);

        // "some" u_der tangent
        ptr->t[0] = float(der_u[0]);
        ptr->t[1] = float(der_u[1]);
        ptr->t[2] = float(der_u[2]);
        ptr->t[3] = 0.0f;
        ptr++;
      }
    }
    m_vertex_buffer->setData(buffer_bytes);
  }

  void PSurfaceGeometry::updateIndices() {

    const int faces = faceCount();

    m_index_attribute->setCount(quint32(faces * 3));


    QByteArray indexBytes;
    indexBytes.resize(faces * int(sizeof(IndexElement)));
    IndexElement* iptr = reinterpret_cast<IndexElement*>(indexBytes.data());

    for (int j = 0; j < m_samples.height() - 1; ++j) {

      const auto row      = j * m_samples.width();
      const auto next_row = (j + 1) * m_samples.width();

      for (int i = 0; i < m_samples.width() - 1; ++i) {

        // tri #1
        iptr->i[0] = quint32(row + i);
        iptr->i[1] = quint32(next_row + i);
        iptr->i[2] = quint32(row + i + 1);
        iptr++;

        // tri #2
        iptr->i[0] = quint32(next_row + i);
        iptr->i[1] = quint32(next_row + i + 1);
        iptr->i[2] = quint32(row + i + 1);
        iptr++;
      }
    }
    m_index_buffer->setData(indexBytes);
  }

  QSize PSurfaceGeometry::samples() const
  {
    return m_samples;
  }

  void PSurfaceGeometry::reSample()
  {
    if (m_samples.height() < 2 or m_samples.width() < 2) return;
    sample();
  }

  void PSurfaceGeometry::setSamples(const QSize &samples)
  {
    if(samples.height() < 2 or samples.width() < 2 ) return;
    if(m_samples == samples) return;

    m_samples = samples;
    sample();
  }

  void PSurfaceGeometry::sample() {

    updateVertices();
    updateIndices();
    emit samplesChanged(m_samples);
  }

  void PSurfaceGeometry::init()
  {

    m_vertex_buffer
      = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, this);
    m_index_buffer
      = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::IndexBuffer, this);


    const int stride = sizeof(VertexElement);   // [ position, normal,
                                               // "some" tangent]


    const int no_vertices = vertexCount();
    const int no_faces = faceCount();


    m_position_attribute = new Qt3DRender::QAttribute(this);
    m_position_attribute->setName(
      Qt3DRender::QAttribute::defaultPositionAttributeName());
    m_position_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_position_attribute->setVertexSize(3);
    m_position_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_position_attribute->setBuffer(m_vertex_buffer);
    m_position_attribute->setByteStride(stride);
    m_position_attribute->setCount(quint32(no_vertices));



    m_normal_attribute = new Qt3DRender::QAttribute(this);
    m_normal_attribute->setName(
      Qt3DRender::QAttribute::defaultNormalAttributeName());
    m_normal_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_normal_attribute->setVertexSize(3);
    m_normal_attribute->setByteOffset(3 * sizeof(float));
    m_normal_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_normal_attribute->setBuffer(m_vertex_buffer);
    m_normal_attribute->setByteStride(stride);
    m_normal_attribute->setCount(quint32(no_vertices));



    m_tangent_attribute = new Qt3DRender::QAttribute(this);
    m_tangent_attribute->setName(
      Qt3DRender::QAttribute::defaultTangentAttributeName());
    m_tangent_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_tangent_attribute->setVertexSize(4);
    m_tangent_attribute->setByteOffset(6 * sizeof(float));
    m_tangent_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_tangent_attribute->setBuffer(m_vertex_buffer);
    m_tangent_attribute->setByteStride(stride);
    m_tangent_attribute->setCount(quint32(no_vertices));



    m_index_attribute = new Qt3DRender::QAttribute(this);
    m_index_attribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    m_index_attribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    m_index_attribute->setBuffer(m_index_buffer);
    m_index_attribute->setCount(quint32(no_faces*3));


    addAttribute(m_position_attribute);
    addAttribute(m_normal_attribute);
    addAttribute(m_tangent_attribute);
    addAttribute(m_index_attribute);
  }

  int PSurfaceGeometry::vertexCount() const
  {
    return m_samples.width() * m_samples.height();
  }

  int PSurfaceGeometry::faceCount() const
  {
    return 2 * (m_samples.width() - 1) * (m_samples.height() - 1);
  }

  QSize PSurfaceMesh::samples() const
  {
    return m_geometry->samples();
  }

  void PSurfaceMesh::reSample()
  {
    return m_geometry->reSample();
  }

  void PSurfaceMesh::setSamples(const QSize &samples)
  {
    m_geometry->setSamples(samples);
  }

}   // namespace gmlib2::qt
