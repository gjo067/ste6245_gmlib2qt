#ifndef GM2QT_PSURFACEGEOMETRY_H
#define GM2QT_PSURFACEGEOMETRY_H


#include "../../sceneobject.h"

// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DRender>

// stl
#include <memory>


namespace gmlib2::qt::parametric
{

  template <typename PSurfaceBase_T>
  class PSurface;

  namespace detail
  {

    class PSurfaceWrapperInterface {
    public:
      PSurfaceWrapperInterface()          = default;
      virtual ~PSurfaceWrapperInterface() = default;

      using EvaluationResult = DMatrixT<VectorT<double, 4>>;
      using SampleResult     = DMatrixT<EvaluationResult>;

      virtual SampleResult sample(const QSize& samples,
                                  const QSize& derivatives) const = 0;
    };

    template <typename PSurfaceBase_T>
    class PSurfaceWrapper final : public PSurfaceWrapperInterface {
    public:
      using PSurface = gmlib2::qt::parametric::PSurface<PSurfaceBase_T>;

      PSurfaceWrapper(PSurface* surface) : m_surface{surface}
      {
        static_assert(
          std::is_same<PSurfaceWrapperInterface::SampleResult,
                       typename PSurfaceBase_T::SampleResult>::value,
          "PSurface does not have expected SampleResult type!");
      }
      ~PSurfaceWrapper() override = default;


      // PolygonSurfaceWrapper interface
    public:
      SampleResult sample(const QSize& samples,
                          const QSize& derivatives) const override
      {
        if (m_surface)
          return m_surface->sample(
            PSizeArray{{size_t(samples.width()), size_t(samples.height())}},
            PSizeArray{
              {size_t(derivatives.width()), size_t(derivatives.height())}});
        else
          return SampleResult();
      }


    private:
      using PSizeArray = typename PSurface::PSpace_SizeArray_Type;
      PSurface* m_surface{nullptr};
    };
  }   // namespace detail










  class PSurfaceGeometry : public Qt3DRender::QGeometry {
    Q_OBJECT
  public:
    template <typename PSurfaceBase_T>
    PSurfaceGeometry(gmlib2::qt::parametric::PSurface<PSurfaceBase_T>* psurf,
                     const QSize& samples = QSize(20, 20),
                     QNode*      parent  = nullptr);
    ~PSurfaceGeometry() override = default;



    QSize samples() const;

  public slots:
    void reSample();
    void setSamples(const QSize& samples);

  signals:
    void samplesChanged(const QSize& samples);

  private:
    struct VertexElement {
      std::array<float, 3> p;
      std::array<float, 3> n;
      std::array<float, 4> t;
    };

    struct IndexElement {
      std::array<quint32, 3> i;

      void print() const
      {
        std::cout << i[0] << ", " << i[1] << ", " << i[2] << std::endl;
        ;
      }
    };

    void sample();
    void updateVertices();
    void updateIndices();
    void init();

    int vertexCount() const;
    int faceCount() const;

    std::unique_ptr<detail::PSurfaceWrapperInterface> m_internal_surface{
      nullptr};

    QSize m_samples{20, 20};

    Qt3DRender::QBuffer*    m_vertex_buffer;
    Qt3DRender::QBuffer*    m_index_buffer;
    Qt3DRender::QAttribute* m_position_attribute;
    Qt3DRender::QAttribute* m_normal_attribute;
    Qt3DRender::QAttribute* m_index_attribute;
    Qt3DRender::QAttribute* m_tangent_attribute;
  };



  template <typename PSurfaceBase_T>
  PSurfaceGeometry::PSurfaceGeometry(
    gmlib2::qt::parametric::PSurface<PSurfaceBase_T>* psurface,
    const QSize& samples, QNode* parent)
    : Qt3DRender::QGeometry(parent),
      m_internal_surface{
        std::make_unique<detail::PSurfaceWrapper<PSurfaceBase_T>>(psurface)},
      m_samples{samples}
  {
    init();
    updateVertices();
    updateIndices();
  }









  class PSurfaceMesh : public Qt3DRender::QGeometryRenderer {
    Q_OBJECT
    using Base = Qt3DRender::QGeometryRenderer;

    Q_PROPERTY(
      QSize samples READ samples WRITE setSamples NOTIFY samplesChanged)

  public:
    //    using Base::Base;
    template <typename PSurfaceBase_T>
    PSurfaceMesh(gmlib2::qt::parametric::PSurface<PSurfaceBase_T>* psurface,
                 const QSize& samples = QSize(20, 20));

    QSize samples() const;


  public slots:
    void reSample();
    void setSamples(const QSize& samples = QSize(20,20));

  signals:
    void samplesChanged(const QSize& samples);

  private:
    PSurfaceGeometry* m_geometry{nullptr};
    bool              m_geometry_initialized{false};
  };



  template <typename PSurfaceBase_T>
  PSurfaceMesh::PSurfaceMesh(
    gmlib2::qt::parametric::PSurface<PSurfaceBase_T>* psurface,
    const QSize&                                      samples)
    : Qt3DRender::QGeometryRenderer(psurface)
  {
    static_assert(std::is_base_of<SceneObject, PSurfaceBase_T>::value, "...");

    m_geometry = new PSurfaceGeometry(psurface, samples, this);
    connect(m_geometry, &PSurfaceGeometry::samplesChanged, this,
            &PSurfaceMesh::samplesChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }

}   // namespace gmlib2::qt::parametric


#endif   // GM2QT_PSURFACEGEOMETRY_H
