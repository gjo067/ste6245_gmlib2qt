#include "customaspect.h"

#include "translatecontroller.h"
#include "translatecontrollerbackend.h"

#include "framecontroller.h"
#include "framecontrollerbackend.h"

#include <QAbstractAspect>
using namespace Qt3DCore;

namespace customaspect
{

  CustomAspect::CustomAspect(QObject* parent)
    : Qt3DCore::QAbstractAspect(parent), m_worker{
                                          CustomJobPtr::create(this)}
  {
    auto translate_mapper
      = QSharedPointer<TranslateControllerBackendMapper>::create(this);
    registerBackendType<TranslateController>(translate_mapper);

    auto frame_mapper
      = QSharedPointer<FrameControllerBackendMapper>::create(this);
    registerBackendType<FrameController>(frame_mapper);
  }


  void CustomAspect::addTranslateControllerBackend(
    Qt3DCore::QNodeId id, TranslateControllerBackend* backend)
  {
    m_translatecontroller_backends.insert(id, backend);
  }

  TranslateControllerBackend*
  CustomAspect::translateControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_translatecontroller_backends.value(id, nullptr);
  }

  TranslateControllerBackend*
  CustomAspect::takeTranslateControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_translatecontroller_backends.take(id);
  }

  const QHash<Qt3DCore::QNodeId, TranslateControllerBackend*>&
  CustomAspect::translateControllerBackends() const
  {
    return m_translatecontroller_backends;
  }



  void CustomAspect::addFrameControllerBackend(
    Qt3DCore::QNodeId id, FrameControllerBackend* backend)
  {
    m_framecontroller_backends.insert(id, backend);
  }

  FrameControllerBackend*
  CustomAspect::frameControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_framecontroller_backends.value(id, nullptr);
  }

  FrameControllerBackend*
  CustomAspect::takeFrameControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_framecontroller_backends.take(id);
  }

  const QHash<Qt3DCore::QNodeId, FrameControllerBackend*>&
  CustomAspect::frameControllerBackends() const
  {
    return m_framecontroller_backends;
  }



  QVector<Qt3DCore::QAspectJobPtr> CustomAspect::jobsToExecute(qint64 time)
  {
    static constexpr auto time_resolution = 1000000000.0;

    const auto time_dt    = time - m_last_time;
    m_last_time           = time;
    const seconds_type dt = seconds_type(time_dt / time_resolution);


    // Update worker
    m_worker->setFrameTimeDt(dt);

    return {m_worker};
  }


}   // namespace simaspect


QT3D_REGISTER_NAMESPACED_ASPECT("customaspect", customaspect, CustomAspect)
