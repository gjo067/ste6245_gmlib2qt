#ifndef GM2QT_CUSTOMASPECT_H
#define GM2QT_CUSTOMASPECT_H

#include "customjob.h"

// qt
#include <Qt3DCore>

namespace customaspect
{

  class TranslateControllerBackend;
  class FrameControllerBackend;

  class CustomAspect : public Qt3DCore::QAbstractAspect {
    Q_OBJECT
  public:
    explicit CustomAspect(QObject* parent = nullptr);

    // Translation controller
    void addTranslateControllerBackend(
      Qt3DCore::QNodeId           id,
      TranslateControllerBackend* TranslateController_backend);
    TranslateControllerBackend*
    translateControllerBackend(Qt3DCore::QNodeId id);
    TranslateControllerBackend*
    takeTranslateControllerBackend(Qt3DCore::QNodeId id);

    const QHash<Qt3DCore::QNodeId, TranslateControllerBackend*>&
    translateControllerBackends() const;

    // Frame controller
    void addFrameControllerBackend(
      Qt3DCore::QNodeId           id,
      FrameControllerBackend* framecontroller_backend);
    FrameControllerBackend*
    frameControllerBackend(Qt3DCore::QNodeId id);
    FrameControllerBackend*
    takeFrameControllerBackend(Qt3DCore::QNodeId id);

    const QHash<Qt3DCore::QNodeId, FrameControllerBackend*>&
    frameControllerBackends() const;

  private:
    QHash<Qt3DCore::QNodeId, TranslateControllerBackend*>
                 m_translatecontroller_backends;

    QHash<Qt3DCore::QNodeId, FrameControllerBackend*>
                 m_framecontroller_backends;

    qint64       m_last_time{0};
    CustomJobPtr m_worker;


    // QAbstractAspect interface
  private:
    QVector<Qt3DCore::QAspectJobPtr> jobsToExecute(qint64 time) override;
  };

}   // namespace customaspect

#endif   // GM2QT_CUSTOMASPECT_H
