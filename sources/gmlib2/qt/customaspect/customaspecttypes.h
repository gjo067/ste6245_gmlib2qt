#ifndef GM2QT_CUSTOMASPECTTYPES_H
#define GM2QT_CUSTOMASPECTTYPES_H


// gmlib2
#include <gmlib2/qt/sceneobject.h>

// qt
#include <QGenericMatrix>

// stl
#include <chrono>

namespace customaspect
{

  // std::ratio<1,1> is implicit
  // default and means seconds
  //
  // Literals are enabled through:
  // using namespace std::chrono_literals;
  using seconds_type = std::chrono::duration<double>;

  // Generic qt matrix
  using QGenMatrix4 = QGenericMatrix<4,4,double>;


}   // namespace customaspect

Q_DECLARE_METATYPE(customaspect::QGenMatrix4)

#endif   // GM2QT_CUSTOMASPECTTYPES_H
