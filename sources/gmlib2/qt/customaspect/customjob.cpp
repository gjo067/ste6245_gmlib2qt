#include "customjob.h"
#include "translatecontrollerbackend.h"
#include "framecontrollerbackend.h"
#include "customaspect.h"

// qt
#include <QDebug>

namespace customaspect
{

  CustomJob::CustomJob(CustomAspect* aspect) : m_aspect{aspect} {}

  void CustomJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void CustomJob::run()
  {
    //  qDebug() << "I'm doing a job for: " << m_dt << " time";

    auto translatecontrollers = m_aspect->translateControllerBackends();
    for (const auto controller : translatecontrollers) {
      if (not controller->isEnabled()) continue;

      controller->translate(m_dt);
    }

    auto framecontrollers = m_aspect->frameControllerBackends();
    for (const auto controller : framecontrollers) {
      if (not controller->isEnabled()) continue;

      controller->compute(m_dt);
    }
  }

}   // namespace customaspect
