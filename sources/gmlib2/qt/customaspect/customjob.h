#ifndef GM2QT_CUSTOMJOB_H
#define GM2QT_CUSTOMJOB_H

#include "customaspecttypes.h"

// qt
#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>


namespace customaspect
{

  class CustomAspect;

  class CustomJob : public Qt3DCore::QAspectJob {
  public:
    CustomJob(CustomAspect* aspect);

    void setFrameTimeDt(seconds_type dt);

  private:
    CustomAspect* m_aspect;
    seconds_type     m_dt;

    // QAspectJob interface
  public:
    void run() override;
  };

  using CustomJobPtr = QSharedPointer<CustomJob>;

}   // namespace customaspect

#endif   // GM2QT_CUSTOMJOB_H
