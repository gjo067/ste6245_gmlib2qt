#include "framecontroller.h"


// gmlib2
#include <gmlib2/qt/scenegraph.h>
using namespace gmlib2::qt;

namespace customaspect
{

  FrameController::FrameController(Qt3DCore::QNode* parent)
    : Qt3DCore::QComponent(parent)
  {
  }

  const QVector3D& FrameController::velocity() const { return m_velocity; }

  void FrameController::setVelocity(const QVector3D& velocity)
  {
    m_velocity = velocity;
    emit velocityChanged(velocity);
  }

  const QVector3D&FrameController::rotAxis() const
  {
    return m_rot_axis;
  }

  void FrameController::setRotAxis(const QVector3D& rot_axis)
  {
    m_rot_axis = rot_axis;
    emit rotAxisChanged(rot_axis);
  }

  double FrameController::rotAngle() const
  {
    return m_rot_angle;
  }

  void FrameController::setRotAngle(double rot_angle)
  {
    m_rot_angle = rot_angle;
    emit rotAngleChanged(rot_angle);
  }

  void
  FrameController::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& change)
  {
    if (change->type() == Qt3DCore::PropertyUpdated) {
      if (const auto e
          = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(change);
          e->propertyName() == QByteArrayLiteral("frame")) {
        const auto frame   = e->value().value<QGenMatrix4>();
        const auto was_blocked = blockNotifications(true);
        emit       differenceFrameComputed(frame);
        blockNotifications(was_blocked);
        return;
      }
    }

    QComponent::sceneChangeEvent(change);
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  FrameController::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<FrameControllerData>::create(this);

    auto& data     = creationChange->data;
    data.velocity  = m_velocity;
    data.rot_angle = m_rot_angle;
    data.rot_axis  = m_rot_axis;
    return std::move(creationChange);
  }

}   // namespace customaspect
