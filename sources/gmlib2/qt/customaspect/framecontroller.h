#ifndef GM2QT_FRAMECONTROLLERCONTROLLER_H
#define GM2QT_FRAMECONTROLLERCONTROLLER_H


#include "customaspecttypes.h"

// gmlib2
#include <gmlib2/qt/sceneobject.h>

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QComponent>
#include <Qt3DCore/QTransform>

namespace customaspect
{


  class FrameController : public Qt3DCore::QComponent {
    Q_OBJECT

    Q_PROPERTY(
      QVector3D velocity READ velocity WRITE setVelocity NOTIFY velocityChanged)
    Q_PROPERTY(
      double rotAngle READ rotAngle WRITE setRotAngle NOTIFY rotAngleChanged)
    Q_PROPERTY(
      QVector3D rotAxis READ rotAxis WRITE setRotAxis NOTIFY rotAxisChanged)
  public:
    FrameController(Qt3DCore::QNode* parent = nullptr);
    ~FrameController() override = default;

    const QVector3D& velocity() const;
    void             setVelocity(const QVector3D& dir);

    const QVector3D& rotAxis() const;
    void             setRotAxis(const QVector3D& rot_axis);

    double rotAngle() const;
    void   setRotAngle(double rot_angle);

  private:
    QVector3D m_velocity{0.0f, 0.0f, 0.0f};
    double    m_rot_angle{0.0};
    QVector3D m_rot_axis{0.0f, 1.0f, 0.0f};

  signals:
    void differenceFrameComputed(const QGenMatrix4& frame);

    void velocityChanged(const QVector3D& dir);
    void rotAngleChanged(double rot_angle);
    void rotAxisChanged(const QVector3D& rot_axis);

    // QNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& change) override;

  private:
    Qt3DCore::QNodeCreatedChangeBasePtr
    createNodeCreationChange() const override;
  };

  struct FrameControllerData {
    QVector3D velocity;
    QVector3D rot_axis;
    double    rot_angle;
  };

}   // namespace customaspect


#endif   // GM2QT_FRAMECONTROLLERCONTROLLER_H
