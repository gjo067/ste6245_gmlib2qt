#include "framecontrollerbackend.h"

#include "framecontroller.h"
#include "customaspect.h"

namespace customaspect
{

  FrameControllerBackend::FrameControllerBackend()
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite)
  {
  }

  void FrameControllerBackend::compute(seconds_type dt)
  {
    using SO3D = gmlib2::ProjectiveSpaceObject<>;
    using Vec = SO3D::Vector_Type;

    const auto dt_sec = dt.count();
    SO3D       space;

//    const auto vel = Vec{double(m_velocity.x()), double(m_velocity.y()),
//                         double(m_velocity.z())};

//    const auto rot_angle = m_rot_angle;
//    const auto rot_axis  = Vec{double(m_rot_axis.x()), double(m_rot_axis.y()),
//                              double(m_rot_axis.z())};

    const auto rot_angle = M_PI;
//    const auto rot_axis  = Vec{1.0, 0.0, 0.0};
//    const auto rot_axis  = Vec{0.0, 1.0, 0.0};
//    const auto rot_axis  = Vec{0.0, 0.0, 1.0};
    const auto rot_axis = Vec{1.0,1.0,0.0};


    // translation
//    space.translate(vel * dt_sec);

    // rotate
    space.rotateParent(rot_angle * dt_sec,rot_axis);

//    std::cout << "FC backend -> compute(...)" << std::endl;
//    std::cout << "  ang: " << rot_angle << std::endl;
//    std::cout << "  axis: " << rot_axis << std::endl;
//    std::cout << std::endl;


    // Convert data to sendable frame
    const auto sf = space.pSpaceFrameParent();
    QGenMatrix4 frame;
    for (size_t i = 0; i < 4; ++i)
      for (size_t j = 0; j < 4; ++j) frame(int(i), int(j)) = sf(i, j);

    // Send data
    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("frame"));
    e->setValue(QVariant::fromValue(frame));
    notifyObservers(e);
  }

  void FrameControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange = qSharedPointerCast<
      Qt3DCore::QNodeCreatedChange<FrameControllerData>>(change);
    const auto& data = typedChange->data;
    m_velocity       = data.velocity;
    m_rot_angle      = data.rot_angle;
    m_rot_axis       = data.rot_axis;
  }

  void FrameControllerBackend::sceneChangeEvent(
    const Qt3DCore::QSceneChangePtr& e)
  {
    qDebug() << "Prop change";

    if (e->type() == Qt3DCore::PropertyUpdated) {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      qDebug() << change->propertyName();
      if (change->propertyName() == QByteArrayLiteral("velocity")) {
        const auto newValue = change->value().value<QVector3D>();
        if (not qFuzzyCompare(newValue, m_velocity)) {
          m_velocity = newValue;
        }
      }

      if (change->propertyName() == QByteArrayLiteral("rotAngle")) {
        const auto newValue = change->value().toDouble();
        if (not qFuzzyCompare(newValue, m_rot_angle)) {
          m_rot_angle = newValue;
        }
      }

      if (change->propertyName() == QByteArrayLiteral("rotAxis")) {
        const auto newValue = change->value().value<QVector3D>();
        if (not qFuzzyCompare(newValue, m_rot_axis)) {
          m_rot_axis = newValue;
        }
      }

    }
    QBackendNode::sceneChangeEvent(e);
  }

  FrameControllerBackendMapper::FrameControllerBackendMapper(
    CustomAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* FrameControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new FrameControllerBackend;
    m_aspect->addFrameControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  FrameControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->frameControllerBackend(id);
  }

  void FrameControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takeFrameControllerBackend(id);
  }

}   // namespace customaspect
