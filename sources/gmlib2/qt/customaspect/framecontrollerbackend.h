#ifndef GM2QT_FRAMECONTROLLERBACKEND_H
#define GM2QT_FRAMECONTROLLERBACKEND_H

#include "customaspecttypes.h"

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QBackendNode>

namespace customaspect
{

  class FrameControllerBackend : public Qt3DCore::QBackendNode {
  public:
    FrameControllerBackend();
    void compute(seconds_type dt);

  private:
    QVector3D m_velocity;
    double    m_rot_angle;
    QVector3D m_rot_axis;

    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
  };


  class CustomAspect;

  class FrameControllerBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit FrameControllerBackendMapper(CustomAspect* aspect);

  private:
    CustomAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;
  };

}   // namespace customaspect

#endif   // GM2QT_FRAMECONTROLLERBACKEND_H
