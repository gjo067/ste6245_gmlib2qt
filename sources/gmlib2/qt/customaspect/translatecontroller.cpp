#include "translatecontroller.h"


// gmlib2
#include <gmlib2/qt/scenegraph.h>
using namespace gmlib2::qt;

namespace customaspect
{

  TranslateController::TranslateController(Qt3DCore::QNode* parent)
    : Qt3DCore::QComponent(parent)
  {
  }

  const QVector3D& TranslateController::velocity() const { return m_velocity; }

  void TranslateController::setVelocity(const QVector3D& velocity)
  {
    m_velocity = velocity;
    emit velocityChanged(velocity);
  }

  void
  TranslateController::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& change)
  {
    if (change->type() == Qt3DCore::PropertyUpdated) {
      const auto e
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(change);
      if (e->propertyName() == QByteArrayLiteral("translation")) {
        const auto translate   = e->value().value<QVector3D>();
        const auto was_blocked = blockNotifications(true);
        emit       translationComputed(translate);
        blockNotifications(was_blocked);
        return;
      }
    }

    QComponent::sceneChangeEvent(change);
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  TranslateController::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<TranslateControllerData>::create(this);

//    if (auto* so = qobject_cast<SceneObject*>(parent()); so)
//      connect(this, &TranslateController::translationComputed, so,
//              &SceneObject::translateGlobalQt);

    auto& data    = creationChange->data;
    data.velocity = m_velocity;
    return std::move(creationChange);
  }

}   // namespace customaspect
