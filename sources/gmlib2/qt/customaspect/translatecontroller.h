#ifndef GM2QT_TRANSLATECONTROLLER_H
#define GM2QT_TRANSLATECONTROLLER_H


#include "customaspecttypes.h"

// gmlib2
#include <gmlib2/qt/sceneobject.h>

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QComponent>
#include <Qt3DCore/QTransform>

namespace customaspect
{

  class TranslateController : public Qt3DCore::QComponent {
    Q_OBJECT

    Q_PROPERTY(
      QVector3D velocity READ velocity WRITE setVelocity NOTIFY velocityChanged)
  public:
    TranslateController(Qt3DCore::QNode* parent = nullptr);
    ~TranslateController() override = default;

    const QVector3D& velocity() const;
    void             setVelocity(const QVector3D& velocity);

  private:
    QVector3D m_velocity;

  signals:
    void translationComputed(const QVector3D& matrix);
    void velocityChanged(const QVector3D& matrix);

    // QNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& change) override;

  private:
    Qt3DCore::QNodeCreatedChangeBasePtr
    createNodeCreationChange() const override;
  };

  struct TranslateControllerData {
    QVector3D velocity;
  };

}   // namespace customaspect


#endif   // GM2QT_TRANSLATECONTROLLER_H
