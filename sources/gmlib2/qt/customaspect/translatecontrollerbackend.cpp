#include "translatecontrollerbackend.h"

#include "translatecontroller.h"
#include "customaspect.h"

namespace customaspect
{

  TranslateControllerBackend::TranslateControllerBackend()
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite)
  {
  }

  void TranslateControllerBackend::translate(seconds_type dt)
  {
    const auto      dt_sec      = float(dt.count());
    const QVector3D translation = m_velocity * dt_sec;

    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("translation"));
    e->setValue(QVariant::fromValue(translation));
    notifyObservers(e);
  }

  void TranslateControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange = qSharedPointerCast<
      Qt3DCore::QNodeCreatedChange<TranslateControllerData>>(change);
    const auto& data = typedChange->data;
    m_velocity       = data.velocity;
  }

  void TranslateControllerBackend::sceneChangeEvent(
    const Qt3DCore::QSceneChangePtr& e)
  {

    if (e->type() == Qt3DCore::PropertyUpdated) {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      qDebug() << change->propertyName();
      if (change->propertyName() == QByteArrayLiteral("velocity")) {
        const auto newValue = change->value().value<QVector3D>();
        if (newValue != m_velocity) {
          m_velocity = newValue;
        }
        return;
      }
    }
    QBackendNode::sceneChangeEvent(e);
  }

  TranslateControllerBackendMapper::TranslateControllerBackendMapper(
    CustomAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* TranslateControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new TranslateControllerBackend;
    m_aspect->addTranslateControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  TranslateControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->translateControllerBackend(id);
  }

  void TranslateControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takeTranslateControllerBackend(id);
  }

}   // namespace customaspect
