#ifndef GM2QT_TRANSLATECONTROLLERBACKEND_H
#define GM2QT_TRANSLATECONTROLLERBACKEND_H

#include "customaspecttypes.h"

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QBackendNode>

namespace customaspect
{

  class TranslateControllerBackend : public Qt3DCore::QBackendNode {
  public:
    TranslateControllerBackend();
    void translate(seconds_type dt);

  private:
    QVector3D m_velocity;

    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
  };


  class CustomAspect;

  class TranslateControllerBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit TranslateControllerBackendMapper(CustomAspect* aspect);

  private:
    CustomAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;
  };

}   // namespace customaspect

#endif   // GM2QT_TRANSLATECONTROLLERBACKEND_H
