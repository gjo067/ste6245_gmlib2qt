#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_CURVES_PCIRCLE_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_CURVES_PCIRCLE_H

#include "../pcurve.h"
#include "../../../sceneobject.h"


// qt
#include <Qt3DExtras>

namespace gmlib2::qt::parametric
{

  class PCircle : public PCurve<gmlib2::parametric::PCircle<SceneObject>> {
    using Base = PCurve<gmlib2::parametric::PCircle<SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(PCurveMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    PCircle(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    // Signal(s)
  signals:
    void radiusChanged();
  };


}   // namespace gmlib2::qt::parametric


#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_CURVES_PCIRCLE_H
