#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_CURVES_PLINE_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_CURVES_PLINE_H

#include "../pcurve.h"
#include "../../../sceneobject.h"

namespace gmlib2::qt::parametric
{

  class PLine : public PCurve<gmlib2::parametric::PLine<SceneObject>> {
    using Base = PCurve<gmlib2::parametric::PLine<SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(PCurveMesh* defaultMesh READ defaultMesh)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    PLine(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    // Signal(s)
  signals:
  };


}   // namespace gmlib2::qt::parametric


#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_CURVES_PLINE_H
