#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PARAMETRICOBJECTS_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PARAMETRICOBJECTS_H

// Parametric point
#include "points/appoint.h"

// Parametric cuves
#include "curves/pcircle.h"
#include "curves/pline.h"

// Parametric surfaces
#include "surfaces/pplane.h"
#include "surfaces/psphere.h"
#include "surfaces/pcylinder.h"
#include "surfaces/ptorus.h"

// Base Parameteric object types
#include "pcurve.h"
#include "psurface.h"
#include "ppolygonsurface.h"
#include "ppolygonsurfaceconstruction.h"

// ... other ...
#include "../../sceneobject.h"

// qt
#include <QObject>


namespace gmlib2::qt::parametric
{


  // Example curves
  using PHermiteCurveP2V2
    = PCurve<gmlib2::parametric::PHermiteCurveP2V2<SceneObject>>;
  using PHermiteCurveP3V2
    = PCurve<gmlib2::parametric::PHermiteCurveP3V2<SceneObject>>;
  using PHermiteCurveP3V3
    = PCurve<gmlib2::parametric::PHermiteCurveP3V3<SceneObject>>;

  // Example surfaces
  using PBicubicCoonsPatch
    = PSurface<gmlib2::parametric::PBicubicCoonsPatch<SceneObject>>;
  using PBilinearCoonsPatch
    = PSurface<gmlib2::parametric::PBilinearCoonsPatch<SceneObject>>;

  // Example polygon surfaces
  using PPolygon3
    = PPolygonSurface<gmlib2::parametric::PPolygon<3, SceneObject>>;
  using PPolygon4
    = PPolygonSurface<gmlib2::parametric::PPolygon<4, SceneObject>>;
  using PPolygon5
    = PPolygonSurface<gmlib2::parametric::PPolygon<5, SceneObject>>;



  // Example polygon surface constructions
  using PPolygonConstruction
    = PPolygonSurfaceConstruction<gmlib2::parametric::polygon_surface_constructions::PPolygon<SceneObject>>;


}   // namespace gmlib2::qt::parametric



#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PARAMETRICOBJECTS_H
