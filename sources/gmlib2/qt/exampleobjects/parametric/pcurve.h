#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PCURVE_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PCURVE_H

#include "../../components/parametric/pcurvegeometry.h"

// qt
#include <Qt3DExtras>

namespace gmlib2::qt::parametric
{

  template <typename PCurve_Type>
  class PCurve : public PCurve_Type {
    using Base = PCurve_Type;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    // Parameter space types
    using PSpacePoint    = typename Base::PSpacePoint;
    using PSizeArray = typename Base::PSizeArray;
    using PBoolArray = typename Base::PBoolArray;

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult
    evaluateGlobal(const PSpacePoint& par, const PSizeArray& no_derivatives,
                   const PBoolArray& from_left = {{true}}) const;

    // Default geometry renderer
    PCurveMesh*                 defaultMesh() const;
    Qt3DExtras::QGoochMaterial* defaultMaterial() const;

  protected:
    void initDefaultMesh();
    void initDefaultMaterial();

  private:
    PCurveMesh*                 m_mesh{nullptr};
    Qt3DExtras::QGoochMaterial* m_material{nullptr};

    // SceneObject interface
  public:
    void initDefaultComponents() override;
  };



  template <typename PCurve_Type>
  typename PCurve<PCurve_Type>::EvaluationResult
  PCurve<PCurve_Type>::evaluateGlobal(const PSpacePoint&    par,
                                      const PSizeArray& no_derivatives,
                                      const PBoolArray& from_left) const
  {
    const auto& global_frame = this->pSpaceFrameGlobal();
    return blaze::map(
      this->evaluate(par, no_derivatives, from_left),
      [global_frame](const auto& ele) { return global_frame * ele; });
  }

  template <typename PCurve_Type>
  PCurveMesh* PCurve<PCurve_Type>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename PCurve_Type>
  Qt3DExtras::QGoochMaterial* PCurve<PCurve_Type>::defaultMaterial() const
  {
    return m_material;
  }

  template <typename PCurve_Type>
  void PCurve<PCurve_Type>::initDefaultMesh()
  {
    m_mesh = new PCurveMesh(this);
    this->addComponent(m_mesh);
  }

  template <typename PCurve_Type>
  void PCurve<PCurve_Type>::initDefaultMaterial()
  {
    m_material = new Qt3DExtras::QGoochMaterial(this);
    m_material->setWarm(QColor("orange"));
    m_material->setCool(QColor("black"));
    m_material->setDiffuse(QColor("purple"));
    this->addComponent(m_material);
  }

  template <typename PCurve_Type>
  void PCurve<PCurve_Type>::initDefaultComponents()
  {
    Base::initDefaultComponents();
    initDefaultMesh();
    initDefaultMaterial();
    this->initDefaultSelectionComponents(defaultMesh());
  }
}   // namespace gmlib2::qt::parametric


#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PCURVE_H
