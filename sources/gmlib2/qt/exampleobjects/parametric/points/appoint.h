#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_POINTS_APPOINT_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_POINTS_APPOINT_H

#include "../ppoint.h"
#include "../../../sceneobject.h"

namespace gmlib2::qt::parametric
{

  class APPoint : public PPoint<gmlib2::parametric::PPoint<SceneObject>> {
    using Base = PPoint<gmlib2::parametric::PPoint<SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(Qt3DExtras::QSphereMesh* defaultMesh READ defaultMesh)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    APPoint(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    // Signal(s)
  signals:
  };


}   // namespace gmlib2::qt::parametric

#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_POINTS_APPOINT_H
