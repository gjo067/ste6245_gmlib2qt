#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PPOLYGONSURFACE_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PPOLYGONSURFACE_H

#include "../../components/parametric/ppolygonsurfacegeometry.h"

// qt
#include <Qt3DExtras>

namespace gmlib2::qt::parametric
{

  template <typename PPolygonSurface_Type>
  class PPolygonSurface : public PPolygonSurface_Type {
    using Base = PPolygonSurface_Type;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    // Paramter space types
    using PSpacePoint    = typename Base::PSpacePoint;
    using PSizeArray = typename Base::PSizeArray;
    using PBoolArray = typename Base::PBoolArray;

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult
    evaluateGlobal(const PSpacePoint& par, const PSizeArray& no_derivatives,
                   const PBoolArray& from_left = {{true, true}}) const;

    // Default geometry renderer
    PPolygonSurfaceMesh*        defaultMesh() const;
    Qt3DExtras::QGoochMaterial* defaultMaterial() const;

  protected:
    void initDefaultMesh();
    void initDefaultMaterial();

  private:
    PPolygonSurfaceMesh*        m_mesh{nullptr};
    Qt3DExtras::QGoochMaterial* m_material{nullptr};

    // SceneObject interface
  public:
    void initDefaultComponents() override;
  };




  template <typename PPolygonSurface_Type>
  typename PPolygonSurface<PPolygonSurface_Type>::EvaluationResult
  PPolygonSurface<PPolygonSurface_Type>::evaluateGlobal(
    const PSpacePoint& par, const PSizeArray& no_derivatives,
    const PBoolArray& from_left) const
  {
    const auto& global_frame = this->pSpaceFrameGlobal();
    return blaze::map(
      this->evaluate(par, no_derivatives, from_left),
      [global_frame](const auto& ele) { return global_frame * ele; });
  }

  template <typename PPolygonSurface_Type>
  PPolygonSurfaceMesh*
  PPolygonSurface<PPolygonSurface_Type>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename PPolygonSurface_Type>
  Qt3DExtras::QGoochMaterial*
  PPolygonSurface<PPolygonSurface_Type>::defaultMaterial() const
  {
    return m_material;
  }

  template <typename PPolygonSurface_Type>
  void PPolygonSurface<PPolygonSurface_Type>::initDefaultMesh()
  {
    m_mesh = new PPolygonSurfaceMesh(this);
//      reinterpret_cast<gmlib2::parametric::PPolygonSurface<SceneObject>*>(this), this);
    this->addComponent(m_mesh);
  }

  template <typename PPolygonSurface_Type>
  void PPolygonSurface<PPolygonSurface_Type>::initDefaultMaterial()
  {
    m_material = new Qt3DExtras::QGoochMaterial(this);
    m_material->setWarm(QColor("orange"));
    m_material->setCool(QColor("black"));
    m_material->setDiffuse(QColor("purple"));
    this->addComponent(m_material);
  }

  template <typename PPolygonSurface_Type>
  void PPolygonSurface<PPolygonSurface_Type>::initDefaultComponents()
  {
    Base::initDefaultComponents();
    initDefaultMesh();
    initDefaultMaterial();
    this->initDefaultSelectionComponents(defaultMesh());
  }

}   // namespace gmlib2::qt::parametric

#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PPOLYGONSURFACE_H
