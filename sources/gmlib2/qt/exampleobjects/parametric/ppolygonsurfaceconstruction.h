#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PPOLYGONSURFACECONSTRUCTION_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PPOLYGONSURFACECONSTRUCTION_H

#include "../../components/parametric/ppolygonsurfaceconstructiongeometry.h"

// qt
#include <Qt3DExtras>


namespace gmlib2::qt::parametric
{

  template <typename PPolygonSurfaceConstruction_Type>
  class PPolygonSurfaceConstruction : public PPolygonSurfaceConstruction_Type {
    using Base = PPolygonSurfaceConstruction_Type;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    // Paramter space types
    using PSpacePoint    = typename Base::PSpacePoint;
    using PSizeArray = typename Base::PSizeArray;
    using PBoolArray = typename Base::PBoolArray;

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult
    evaluateGlobal(const PSpacePoint& par, const PSizeArray& no_derivatives,
                   const PBoolArray& from_left = {{true, true}}) const;

    // Default geometry renderer
    PPolygonSurfaceConstructionMesh*        defaultMesh() const;
    Qt3DExtras::QGoochMaterial* defaultMaterial() const;

  protected:
    void initDefaultMesh();
    void initDefaultMaterial();

  private:
    PPolygonSurfaceConstructionMesh*        m_mesh{nullptr};
    Qt3DExtras::QGoochMaterial* m_material{nullptr};

    // SceneObject interface
  public:
    void initDefaultComponents() override;
  };




  template <typename PPolygonSurfaceConstruction_Type>
  typename PPolygonSurfaceConstruction<PPolygonSurfaceConstruction_Type>::EvaluationResult
  PPolygonSurfaceConstruction<PPolygonSurfaceConstruction_Type>::evaluateGlobal(
    const PSpacePoint& par, const PSizeArray& no_derivatives,
    const PBoolArray& from_left) const
  {
    const auto& global_frame = this->pSpaceFrameGlobal();
    return blaze::map(
      this->evaluate(par, no_derivatives, from_left),
      [global_frame](const auto& ele) { return global_frame * ele; });
  }

  template <typename PPolygonSurfaceConstruction_Type>
  PPolygonSurfaceConstructionMesh*
  PPolygonSurfaceConstruction<PPolygonSurfaceConstruction_Type>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename PPolygonSurfaceConstruction_Type>
  Qt3DExtras::QGoochMaterial*
  PPolygonSurfaceConstruction<PPolygonSurfaceConstruction_Type>::defaultMaterial() const
  {
    return m_material;
  }

  template <typename PPolygonSurfaceConstruction_Type>
  void PPolygonSurfaceConstruction<PPolygonSurfaceConstruction_Type>::initDefaultMesh()
  {
    m_mesh = new PPolygonSurfaceConstructionMesh(this);
//      reinterpret_cast<gmlib2::parametric::PPolygonSurfaceConstruction<SceneObject>*>(this), this);
    this->addComponent(m_mesh);
  }

  template <typename PPolygonSurfaceConstruction_Type>
  void PPolygonSurfaceConstruction<PPolygonSurfaceConstruction_Type>::initDefaultMaterial()
  {
    m_material = new Qt3DExtras::QGoochMaterial(this);
    m_material->setWarm(QColor("orange"));
    m_material->setCool(QColor("black"));
    m_material->setDiffuse(QColor("purple"));
    this->addComponent(m_material);
  }

  template <typename PPolygonSurfaceConstruction_Type>
  void PPolygonSurfaceConstruction<PPolygonSurfaceConstruction_Type>::initDefaultComponents()
  {
    Base::initDefaultComponents();
    initDefaultMesh();
    initDefaultMaterial();
    this->initDefaultSelectionComponents(defaultMesh());
  }

}   // namespace gmlib2::qt::parametric

#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PPOLYGONSURFACECONSTRUCTION_H
