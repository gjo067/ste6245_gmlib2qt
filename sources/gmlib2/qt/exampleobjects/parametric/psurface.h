#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PSURFACE_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PSURFACE_H

#include "../../components/parametric/psurfacegeometry.h"

// qt
#include <Qt3DExtras>

namespace gmlib2::qt::parametric
{

  template <typename PSurface_Type>
  class PSurface : public PSurface_Type {
    using Base = PSurface_Type;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    // Parameter space types
    using PSpacePoint    = typename Base::PSpacePoint;
    using PSizeArray = typename Base::PSizeArray;
    using PBoolArray = typename Base::PBoolArray;

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult
    evaluateGlobal(const PSpacePoint& par, const PSizeArray& no_derivatives,
                   const PBoolArray& from_left = {{true, true}}) const;

    // Default geometry renderer
    PSurfaceMesh*               defaultMesh() const;
    Qt3DExtras::QGoochMaterial* defaultMaterial() const;


  protected:
    void initDefaultMesh(const QSize& samples);
    void initDefaultMaterial();

  private:
    PSurfaceMesh*               m_mesh{nullptr};
    Qt3DExtras::QGoochMaterial* m_material{nullptr};

    // SceneObject interface
  public:
    void initDefaultComponents() override;
  };




  template <typename PSurface_Type>
  typename PSurface<PSurface_Type>::EvaluationResult
  PSurface<PSurface_Type>::evaluateGlobal(const PSpacePoint&    par,
                                          const PSizeArray& no_derivatives,
                                          const PBoolArray& from_left) const
  {
    auto eval_res = this->evaluate(par, no_derivatives, from_left);
    auto global_hframe = this->pSpaceFrameGlobal();

    EvaluationResult res
      = blaze::map(eval_res, [&global_hframe](const auto& ele) {
          return blaze::evaluate(global_hframe * ele);
        });
    return res;
  }

  template <typename PSurface_Type>
  PSurfaceMesh* PSurface<PSurface_Type>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename PSurface_Type>
  Qt3DExtras::QGoochMaterial* PSurface<PSurface_Type>::defaultMaterial() const
  {
    return m_material;
  }

  template <typename PSurface_Type>
  void PSurface<PSurface_Type>::initDefaultMesh(const QSize& samples)
  {
    m_mesh = new PSurfaceMesh(this,samples);
    this->addComponent(m_mesh);
  }

  template <typename PSurface_Type>
  void PSurface<PSurface_Type>::initDefaultMaterial()
  {
    m_material = new Qt3DExtras::QGoochMaterial(this);
    m_material->setWarm(QColor("orange"));
    m_material->setCool(QColor("black"));
    m_material->setDiffuse(QColor("purple"));
    this->addComponent(m_material);
  }

  template <typename PSurface_Type>
  void PSurface<PSurface_Type>::initDefaultComponents()
  {
    Base::initDefaultComponents();
    initDefaultMesh(QSize(5,5));
    initDefaultMaterial();
    this->initDefaultSelectionComponents(defaultMesh());
  }

}   // namespace gmlib2::qt::parametric

#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_PSURFACE_H
