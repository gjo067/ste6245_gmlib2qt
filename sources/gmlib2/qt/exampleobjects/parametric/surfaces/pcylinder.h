#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PCYLINDER_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PCYLINDER_H

#include "../psurface.h"
#include "../../../sceneobject.h"

namespace gmlib2::qt::parametric
{

    class PCylinder : public PSurface<gmlib2::parametric::PCylinder<SceneObject>> {
      using Base = PSurface<gmlib2::parametric::PCylinder<SceneObject>>;
      Q_OBJECT

        // clang-format off
        Q_PROPERTY(PSurfaceMesh* defaultMesh READ defaultMesh)
        // clang-format on

        // constructor(s)
    public:
        template <typename... Ts>
        PCylinder(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
        {
          initDefaultComponents();
        }

    signals:
    };
}

#endif // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PCYLINDER_H
