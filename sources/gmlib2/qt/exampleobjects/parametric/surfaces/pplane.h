#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PPLANE_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PPLANE_H

#include "../psurface.h"
#include "../../../sceneobject.h"

namespace gmlib2::qt::parametric
{

  class PPlane : public PSurface<gmlib2::parametric::PPlane<SceneObject>> {
    using Base = PSurface<gmlib2::parametric::PPlane<SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(PSurfaceMesh* defaultMesh READ defaultMesh)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    PPlane(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    // Signal(s)
  signals:
  };

}   // namespace gmlib2::qt::parametric


#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PPLANE_H
