#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PSPHERE_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PSPHERE_H

#include "../psurface.h"
#include "../../../sceneobject.h"

namespace gmlib2::qt::parametric
{

  class PSphere : public PSurface<gmlib2::parametric::PSphere<SceneObject>> {
    using Base = PSurface<gmlib2::parametric::PSphere<SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(PSurfaceMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    PSphere(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    // Signal(s)
  signals:
    void radiusChanged();
  };


}   // namespace gmlib2::qt::parametric


#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PSPHERE_H
