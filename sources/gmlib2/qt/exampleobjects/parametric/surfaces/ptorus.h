#ifndef GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PTORUS_H
#define GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PTORUS_H

#include "../psurface.h"
#include "../../../sceneobject.h"

namespace gmlib2::qt::parametric
{

  class PTorus : public PSurface<gmlib2::parametric::PTorus<SceneObject>> {
    using Base = PSurface<gmlib2::parametric::PTorus<SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(PSurfaceMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY( double wheelRadius
                MEMBER m_wheelradius NOTIFY wheelRadiusChanged)
    Q_PROPERTY( double tubeRadiusOne
                MEMBER m_tuberadius1 NOTIFY tubeRadiusOneChanged)
    Q_PROPERTY( double tubeRadiusTwo
                MEMBER m_tuberadius2 NOTIFY tubeRadiusTwoChanged)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    PTorus(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    // Signal(s)
  signals:
    void wheelRadiusChanged();
    void tubeRadiusOneChanged();
    void tubeRadiusTwoChanged();
  };


}   // namespace gmlib2::qt::parametric


#endif   // GM2QT_EXAMPLEOBJECTS_PARAMETRIC_SURFACES_PTORUS_H
