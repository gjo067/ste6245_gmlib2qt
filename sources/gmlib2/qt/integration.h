#ifndef GM2QT_INTEGRATION_H
#define GM2QT_INTEGRATION_H

#include "scenegraph.h"
#include "components/parametric/psurfacegeometry.h"
#include "utils/traditionalgmlibcameracontroller.h"
#include "exampleobjects/parametric/parametricobjects.h"

// qt
#include <QEvent>

namespace gmlib2::qt
{

  namespace customevent
  {
    const auto GMlib2Qt_SceneObjectConstrEventType
      = QEvent::registerEventType();
  }   // namespace customevent


  namespace init
  {
    namespace detail
    {
      constexpr auto qt_registertype_uri = "com.uit.GMlib2Qt";
    }   // namespace detail




    void registerQmlTypes()
    {

      // Createable types
      qmlRegisterType<Scenegraph>(detail::qt_registertype_uri, 1, 0,
                                  "SceneRootEntity");
      qmlRegisterType<SceneObject>(detail::qt_registertype_uri, 1, 0,
                                   "SceneObject");
      qmlRegisterType<TraditionalGMlibCameraController>(
        detail::qt_registertype_uri, 1, 0, "TraditionalGMlibCameraController");



      // UnCreateable types
      qmlRegisterUncreatableType<SceneModel>(detail::qt_registertype_uri, 1, 0,
                                             "SceneModel",
                                             "The scenegraph owns this");
      qmlRegisterUncreatableType<SceneObjectComponentModel>(
        detail::qt_registertype_uri, 1, 0, "SceneObjectComponentModel",
        "It lives its own life");
      qmlRegisterUncreatableType<parametric::PCurveMesh>(
        detail::qt_registertype_uri, 1, 0, "PCurveMesh",
        "Provides a 'view'...");
      qmlRegisterUncreatableType<parametric::PSurfaceMesh>(
        detail::qt_registertype_uri, 1, 0, "PSurfaceMesh",
        "Provides a 'view'...");
    }

    void registerExampleObjectsAsQmlTypes()
    {
      // parametric point
      qmlRegisterType<parametric::APPoint>(detail::qt_registertype_uri, 1, 0,
                                           "PPoint");

      // parametric curves
      qmlRegisterType<parametric::PCircle>(detail::qt_registertype_uri, 1, 0,
                                           "PCircle");
      qmlRegisterType<parametric::PLine>(detail::qt_registertype_uri, 1, 0,
                                         "PLine");

      // parametric surfaces
      qmlRegisterType<parametric::PPlane>(detail::qt_registertype_uri, 1, 0,
                                          "PPlane");
      qmlRegisterType<parametric::PSphere>(detail::qt_registertype_uri, 1, 0,
                                           "PSphere");
      qmlRegisterType<parametric::PTorus>(detail::qt_registertype_uri, 1, 0,
                                          "PTorus");
      qmlRegisterType<parametric::PCylinder>(detail::qt_registertype_uri, 1, 0,
                                          "PCylinder");
    }

    void init()
    {
      registerQmlTypes();
      registerExampleObjectsAsQmlTypes();
    }
  }   // namespace init
}   // namespace gmlib2::qt

#endif   // GM2QT_INTEGRATION_H
