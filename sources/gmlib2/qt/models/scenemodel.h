#ifndef GM2QT_SCENEMODEL_H
#define GM2QT_SCENEMODEL_H


#include "../components/parametric/pcurvegeometry.h"
#include "../components/parametric/psurfacegeometry.h"

#include "sceneobjectcomponentmodel.h"

// gmlib2
#include <gmlib2.h>

// qt
#include <QAbstractItemModel>
#include <Qt3DCore>

// stl
#include <memory>



namespace gmlib2::qt
{

  class Scenegraph;

  class SceneModel : public QAbstractItemModel {
    Q_OBJECT


  public:
    using SceneObjectVector = QVector<SceneObject*>;


    enum ModelRoles {
      ItemRole = Qt::UserRole + 1,
      NameRole,
      MeshRole,
      TransformRole,
      ComponentModelRole,
      MaterialRole,
      SelectedRole,
      SOUserRole
    };
    Q_ENUM(ModelRoles)
    enum class ModelTypes { Curve, Surface };

    ~SceneModel() override = default;

  public slots:
    void update()
    {
      beginResetModel();
      endResetModel();
    }

    void updateParentChanged(QObject* /*parent*/)
    {
      beginResetModel();
      endResetModel();
    }

  public:
    Scenegraph* m_scenegraph;

  private:
    ModelTypes m_model_type{ModelTypes::Surface};
    size_t     m_no_samples_u{100};
    size_t     m_no_samples_v{100};

  private:
    SceneModel(Scenegraph* scenegraph);

    // Interface: QAbstractItemModel
  public:
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant      headerData(int section, Qt::Orientation orientation,
                             int role) const override;
    QVariant      data(const QModelIndex& index, int role) const override;
    QModelIndex   index(int row, int column,
                        const QModelIndex& parent) const override;
    QModelIndex   parent(const QModelIndex& child) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    QHash<int, QByteArray> roleNames() const override;

    QModelIndexList match(int role, const QVariant& value) const;
    QModelIndexList match(const QModelIndex& start, int role,
                          const QVariant& value, int hits = 1,
                          Qt::MatchFlags flags
                          = Qt::MatchFlags(Qt::MatchStartsWith
                                           | Qt::MatchWrap)) const override;

    SceneObject* sceneObjectFromIndex(const QModelIndex& index) const;

  private:
    SceneObjectVector
                      toSceneObjects(const Qt3DCore::QNodeVector& objects) const;
    SceneObjectVector sceneObjectChildren(const QModelIndex& index) const;
    SceneObjectVector sceneObjectChildren(const SceneObject* sceneobject) const;
    SceneObjectVector sceneObjectChildren(const Scenegraph* scenegraph) const;
    QModelIndexList   modelIndexChildren(const QModelIndex& index) const;


    // Friends
    friend class Scenegraph;
  };

}   // namespace gmlib2::qt

#endif   // GM2QT_SCENEMODEL_H
