#include "scenegraph.h"

namespace gmlib2::qt
{

  Scenegraph::Scenegraph(QNode* parent)
    : QEntity(parent), m_scenemodel(this), m_select_layer(this)
  {
  }

  SceneModel* Scenegraph::scenemodel() { return &m_scenemodel; }

  Qt3DRender::QLayer* Scenegraph::selectLayer() { return &m_select_layer; }

  void Scenegraph::lazyPrepare()
  {
    for (const auto& so_dirty_pair : m_dirty_sceneobjects_global_matrix) {

      auto* so = so_dirty_pair.second;

      if (not so->m_global_frames_dirty_flag) continue;

      if (auto* so_parent = qobject_cast<SceneObject*>(so->parent()); so_parent)
        so->updateGlobalFrames(so_parent);
      else
        so->updateGlobalFrames(this);
    }
    m_dirty_sceneobjects_global_matrix.clear();
  }

  const SceneObject::SceneObjectVector& Scenegraph::sceneObjectChildren() const
  {
    return m_sceneobject_children;
  }

  void Scenegraph::markGlobalMatrixDirty(SceneObject* sceneobject)
  {
    m_dirty_sceneobjects_global_matrix.insert(
      {sceneobject->m_scene_depth_pos, sceneobject});
  }

  void Scenegraph::childEvent(QChildEvent* event)
  {
    const auto event_type        = event->type();
    auto*      child             = event->child();
    auto*      sceneobject_child = qobject_cast<SceneObject*>(child);

    if (sceneobject_child and event_type == QEvent::ChildAdded) {
      m_sceneobject_children.push_back(sceneobject_child);
      sceneobject_child->updateSceneDepthInfo(0);
    }
    else if (sceneobject_child and event_type == QEvent::ChildRemoved) {
      m_sceneobject_children.removeOne(sceneobject_child);
      sceneobject_child->updateSceneDepthInfo(-1);
    }
  }

}   // namespace gmlib2::qt
