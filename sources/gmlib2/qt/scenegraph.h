#ifndef GM2QT_SCENEGRAPH_H
#define GM2QT_SCENEGRAPH_H


#include "models/scenemodel.h"
//#include "components/parametric/pcurvegeometry.h"
//#include "components/parametric/psurfacegeometry.h"

// qt
#include <QObject>

// blaze
#include <gmlib2.h>

// qt
#include <Qt3DCore>
#include <Qt3DRender>

// stl
#include <memory>
#include <vector>
#include <iostream>



#include "sceneobject.h"


namespace gmlib2::qt
{

  class Scenegraph : public Qt3DCore::QEntity {
    Q_OBJECT

    Q_PROPERTY(SceneModel* scenemodel READ scenemodel CONSTANT)
    Q_PROPERTY(Qt3DRender::QLayer* select_layer READ selectLayer CONSTANT)
  public:
    explicit Scenegraph(QNode* parent = nullptr);

    void simulate(std::chrono::nanoseconds dt);

    SceneModel*         scenemodel();
    Qt3DRender::QLayer* selectLayer();

    void                                  lazyPrepare();
    const SceneObject::SceneObjectVector& sceneObjectChildren() const;

    QSet<QPair<qint64, SceneObject*>> m_dirty_sceneobjects_global_matrix;

  private:
    SceneModel                     m_scenemodel;
    SceneObject::SceneObjectVector m_sceneobject_children;
    Qt3DRender::QLayer             m_select_layer;


    void markGlobalMatrixDirty(SceneObject* sceneobject);

    // Friends
    friend class SceneObject;
    friend class SceneModel;

    // QObject interface
  protected:
    void childEvent(QChildEvent* event) override;
  };


}   // namespace gmlib2::qt

#endif   // GM2QT_SCENEGRAPH_H
