#include "sceneobject.h"

#include "scenegraph.h"
#include "utils/typeconversion.h"

// qt
#include <QCoreApplication>
#include <Qt3DRender>
#include <Qt3DExtras>

// stl
#include <sstream>
#include <iostream>



namespace gmlib2::qt
{



  SceneObject::SceneObject(Qt3DCore::QNode* parent)
    : EntityBase(parent), EmbedBase()
  {
    // "try to" get scenegraph from parent
    setSceneGraph(getSceneGraphFromParent(parent));
    markGlobalFrameDirty();

    connect(this, &EntityBase::parentChanged, this,
            &SceneObject::handleParentChanged);

    m_transform = new Qt3DCore::QTransform(this);
    connect(this, &SceneObject::pSpaceFrameParentChanged, m_transform,
            &Qt3DCore::QTransform::setMatrix);
    addComponent(m_transform);
  }

  void SceneObject::initDefaultSelectionComponents(
    Qt3DRender::QGeometryRenderer* renderer)
  {
    // Object picker
    m_objectpicker = new Qt3DRender::QObjectPicker(this);
    connect(m_objectpicker, &Qt3DRender::QObjectPicker::clicked, this,
            &SceneObject::pick);
    addComponent(m_objectpicker);

    // Selection object
    m_selected_object = new QEntity(this);
    m_selected_object->setEnabled(false);

    // Selection object renderer
    m_selected_object->addComponent(renderer);

    // Selection object material
    m_selected_material
      = new Qt3DExtras::QPhongAlphaMaterial(m_selected_object);
    m_selected_material->setDiffuse(QColor("#FF0000"));
    m_selected_material->setAmbient(QColor("#FF0000"));
    m_selected_material->setAlpha(0.1f);
    m_selected_object->addComponent(m_selected_material);
  }

  bool SceneObject::selected() const { return m_selected_flag; }

  void SceneObject::setSelected(bool selected_flag)
  {
    m_selected_flag = selected_flag;
    m_selected_object->setEnabled(selected_flag);
    emit selectedChanged(selected_flag);
  }

  void SceneObject::toggleSelected() { setSelected(not m_selected_flag); }

  QVector3D SceneObject::directionAxisGlobalQt() const
  {
    const auto& d = directionAxisGlobal();
    return {float(d[0]), float(d[1]), float(d[2])};
  }

  QVector3D SceneObject::sideAxisGlobalQt() const
  {
    const auto& s = sideAxisGlobal();
    return {float(s[0]), float(s[1]), float(s[2])};
  }

  QVector3D SceneObject::upAxisGlobalQt() const
  {
    const auto& u = upAxisGlobal();
    return {float(u[0]), float(u[1]), float(u[2])};
  }

  QVector3D SceneObject::frameOriginGlobalQt() const
  {
    const auto& p = frameOriginGlobal();
    return {float(p[0]), float(p[1]), float(p[2])};
  }

  void SceneObject::pick(Qt3DRender::QPickEvent* pick)
  {
    pick->setAccepted(true);
    toggleSelected();
  }

  Scenegraph* SceneObject::sceneGraph() const { return m_scenegraph; }

  void SceneObject::setSceneGraph(Scenegraph* scenegraph)
  {
    m_scenegraph = scenegraph;
    for (auto* child : sceneObjectChildren())
      child->setSceneGraph(m_scenegraph);
    markGlobalFrameDirty();

    // Select mechanism sync
    if (m_selected_object) {
      if (m_select_layer) m_selected_object->removeComponent(m_select_layer);
      if (not m_scenegraph)
        m_select_layer = nullptr;
      else {
        m_select_layer = m_scenegraph->selectLayer();
        m_selected_object->addComponent(m_select_layer);
      }
    }

    // Scene model -- this responsibility must be mvoed to the scenegraph
    if (m_scenegraph) m_scenegraph->scenemodel()->update();
  }

    void SceneObject::rotateLocalQt(float angle_in_radians, const QVector3D& a)
    {
      rotateLocal(double(angle_in_radians),
                  Vector{double(a[0]), double(a[1]), double(a[2])});
    }

  void SceneObject::rotateParentQt(float angle_in_radians, const QVector3D& a)
  {
    rotateParent(double(angle_in_radians),
                 Vector{double(a[0]), double(a[1]), double(a[2])});
  }

  void SceneObject::rotateGlobalQt(float angle_in_radians, const QVector3D& a)
  {
    rotateGlobal(double(angle_in_radians),
                 Vector{double(a[0]), double(a[1]), double(a[2])});
  }

  void SceneObject::translateLocalQt(const QVector3D& v)
  {
    const Vector vec{double(v[0]), double(v[1]), double(v[2])};
    translateLocal(vec);
  }

  void SceneObject::translateParentQt(const QVector3D& v)
  {
    const Vector vec{double(v[0]), double(v[1]), double(v[2])};
    translateParent(vec);
  }

  void SceneObject::translateGlobalQt(const QVector3D& v)
  {
    const Vector vec{double(v[0]), double(v[1]), double(v[2])};
    translateGlobal(vec);
  }

  void SceneObject::setFrameParent(
    const Vector& dir, const Vector& up, const Point& origin)
  {
    EmbedBase::setFrameParent(dir,up,origin);
    endTransform();
  }


  void SceneObject::setFrameParentQt(const QVector3D& dir, const QVector3D& up,
                                     const QVector3D& pos)
  {
    auto q3ToVec = [](const auto& qv) {
      using U = Unit_Type;
      return Vector_Type{U(qv.x()), U(qv.y()), U(qv.z())};
    };

    setFrameParent(q3ToVec(dir), q3ToVec(up), q3ToVec(pos));
    endTransform();
  }

  void SceneObject::setFrameGlobalQt(const QVector3D &dir, const QVector3D &up, const QVector3D &pos)
  {
    auto q3ToVec = [](const auto& qv) {
      using U = Unit_Type;
      return Vector_Type{U(qv.x()), U(qv.y()), U(qv.z())};
    };

    setFrameGlobal(q3ToVec(dir), q3ToVec(up), q3ToVec(pos));
    endTransform();
  }

  void SceneObject::translateLocal(const Vector& v)
  {
    EmbedBase::translateLocal(v);
    endTransform();
  }

  void SceneObject::translateParent(const SceneObject::Vector& v)
  {
    EmbedBase::translateParent(v);
    endTransform();
  }

  void SceneObject::translateGlobal(const SceneObject::Vector& v)
  {
    translateParent(Vector{blaze::inv(vSpaceFrameGlobal()) * v});
  }

  void SceneObject::handleParentChanged(QObject* parent)
  {
    setSceneGraph(getSceneGraphFromParent(parent));
  }

  void SceneObject::rotateLocal(double angle_in_radians, const Vector& axis)
  {
    EmbedBase::rotateLocal(angle_in_radians, axis);
    endTransform();
  }

  void SceneObject::rotateParent(double angle_in_radians, const Vector& axis)
  {
    EmbedBase::rotateParent(angle_in_radians, axis);
    endTransform();
  }

  void SceneObject::rotateGlobal(double angle_in_radians, const Vector& axis)
  {
    rotateParent(angle_in_radians, Vector{blaze::inv(vSpaceFrameParent()) * axis});
  }

  const SceneObject::Vector SceneObject::directionAxisGlobal() const
  {
    return parentVSpaceFrameGlobal() * this->directionAxisParent();
  }

  const SceneObject::Vector SceneObject::sideAxisGlobal() const
  {
    return parentVSpaceFrameGlobal() * this->sideAxisParent();
  }

  const SceneObject::Vector SceneObject::upAxisGlobal() const
  {
    return parentVSpaceFrameGlobal() * this->upAxisParent();
  }

  const SceneObject::Point SceneObject::frameOriginGlobal() const
  {
    return blaze::subvector<0UL, VectorDim>(parentPSpaceFrameGlobal()
                                            * this->frameOriginParentH());
  }

  const SceneObject::VectorH SceneObject::directionAxisGlobalH() const
  {
    return parentPSpaceFrameGlobal() * this->directionAxisParentH();
  }

  const SceneObject::VectorH SceneObject::sideAxisGlobalH() const
  {
    return parentPSpaceFrameGlobal() * this->sideAxisParentH();
  }

  const SceneObject::VectorH SceneObject::upAxisGlobalH() const
  {
    return parentPSpaceFrameGlobal() * this->upAxisParentH();
  }

  const SceneObject::PointH SceneObject::frameOriginGlobalH() const
  {
    return parentPSpaceFrameGlobal() * this->frameOriginParentH();
  }

  void SceneObject::setFrameGlobal(const Vector& dir, const Vector& up,
                                   const Point& origin)
  {
    // Invert parent's frame wrt. global space
    const auto parent_pspace_frame_global_inv
      = ASFrameH(blaze::inv(m_parent_pspace_frame_global));

    // Move global frame components into wrt. parent space
    const auto gdirh = parent_pspace_frame_global_inv
                       * VectorH{dir[0], dir[1], dir[2], Unit_Type(0)};
    const auto guph = parent_pspace_frame_global_inv
                      * VectorH{up[0], up[1], up[2], Unit_Type(0)};
    const auto goriginh
      = parent_pspace_frame_global_inv
        * VectorH{origin[0], origin[1], origin[2], Unit_Type(1)};

    // Set frame wrt. parent space
    setFrameParent(blaze::subvector<0UL, 3UL>(gdirh),
                   blaze::subvector<0UL, 3UL>(guph),
                   blaze::subvector<0UL, 3UL>(goriginh));
  }


  const SceneObject::Frame SceneObject::parentVSpaceFrameGlobal() const
  {
    using ReturnType = const SceneObject::Frame;
    return ReturnType(
      blaze::eval(blaze::submatrix<0UL, 0UL, VectorDim, FrameDim>(
        parentPSpaceFrameGlobal())));
  }

  const SceneObject::Frame SceneObject::vSpaceFrameGlobal() const
  {
    using ReturnType = const SceneObject::Frame;
    return ReturnType(blaze::eval(
      blaze::submatrix<0UL, 0UL, VectorDim, FrameDim>(pSpaceFrameGlobal())));
  }

  const SceneObject::ASFrameH SceneObject::parentPSpaceFrameGlobal() const
  {
    lazyPrepare();
    return m_parent_pspace_frame_global;
  }

  const SceneObject::ASFrameH SceneObject::pSpaceFrameGlobal() const
  {
    lazyPrepare();
    return m_pspace_frame_global;
  }

  void SceneObject::endTransform()
  {
    markGlobalFrameDirty();
    emit pSpaceFrameParentChanged(toQtMatrix4x4(pSpaceFrameParent()));
  }

  void SceneObject::markGlobalFrameDirty()
  {
    m_global_frames_dirty_flag = {true};
    if (m_scenegraph) m_scenegraph->markGlobalMatrixDirty(this);
  }

  void SceneObject::updateSceneDepthInfo(qint64 parent_scene_depth_pos)
  {
    m_scene_depth_pos = parent_scene_depth_pos + 1;
    for (auto* child : sceneObjectChildren())
      child->updateSceneDepthInfo(m_scene_depth_pos);
  }

  void SceneObject::lazyPrepare() const
  {
    if (m_scenegraph) m_scenegraph->lazyPrepare();
  }

  void SceneObject::updateGlobalFrames(SceneObject* parent)
  {
    m_pspace_frame_global = m_parent_pspace_frame_global = parent->m_pspace_frame_global;
    m_pspace_frame_global = m_pspace_frame_global * pSpaceFrameParent();

    endGlobalFramesUpdate();
    for (auto* child : sceneObjectChildren()) child->updateGlobalFrames(this);
  }

  void SceneObject::updateGlobalFrames(Scenegraph* /*parent*/)
  {
    m_pspace_frame_global
      = spaces::projectivespace::identityFrame<EmbedBase::EmbedSpace>()
        * pSpaceFrameParent();
    endGlobalFramesUpdate();
    for (auto* child : sceneObjectChildren()) child->updateGlobalFrames(this);
  }

  SceneObject::SceneObjectVector SceneObject::sceneObjectChildren() const
  {
    SceneObjectVector sceneobject_child_list;
    const auto&       child_list = childNodes();
    sceneobject_child_list.reserve(child_list.size());
    for (auto* c : child_list) {
      if (auto* so = qobject_cast<SceneObject*>(c); so)
        sceneobject_child_list.push_back(so);
    }
    return sceneobject_child_list;
  }

  Scenegraph* SceneObject::getSceneGraphFromParent(QObject* parent)
  {
    // If parent is not, so are also not its scenegraph
    if (not parent) return nullptr;

    // If parent is scenegraph - voala
    if (auto* scenegraph = dynamic_cast<Scenegraph*>(parent); scenegraph)
      return scenegraph;

    // If parent is scenegraph object ask for its parent
    if (auto* sceneobject = dynamic_cast<SceneObject*>(parent); sceneobject)
      return sceneobject->sceneGraph();

    // else recurse up
    return getSceneGraphFromParent(parent->parent());
  }

  void SceneObject::endGlobalFramesUpdate()
  {
    m_global_frames_dirty_flag = {false};
    emit pSpaceFrameGlobalChanged(toQtMatrix4x4(m_pspace_frame_global));
  }

}   // namespace gmlib2::qt
