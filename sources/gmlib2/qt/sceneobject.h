#ifndef GM2QT_SCENEOBJECT_H
#define GM2QT_SCENEOBJECT_H


// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DCore>
namespace Qt3DRender
{
  class QGeometryRenderer;
  class QObjectPicker;
  class QPickEvent;
  class QLayer;
}   // namespace Qt3DRender
namespace Qt3DExtras
{
  class QPhongAlphaMaterial;
}


namespace gmlib2::qt
{



  class Scenegraph;

  class SceneObject : public Qt3DCore::QEntity,
                      public gmlib2::ProjectiveSpaceObject<> {
    Q_OBJECT

    Q_PROPERTY(
      bool selected READ selected WRITE setSelected NOTIFY selectedChanged)

    // Friends
    friend class Scenegraph;
    friend class SceneModel;

  private:
    // Base clases
    using EntityBase = Qt3DCore::QEntity;
    using EmbedBase  = gmlib2::ProjectiveSpaceObject<>;

    // Types
    using SceneObjectVector = QVector<SceneObject*>;

  public:
    // Aggregate dimensions
    static constexpr auto FrameDim   = EmbedSpace::FrameDim;
    static constexpr auto VectorDim  = EmbedSpace::VectorDim;
    static constexpr auto ASFrameDim = EmbedSpace::ASFrameDim;
    static constexpr auto VectorHDim = EmbedSpace::VectorHDim;

    // Aggregate types
    using Unit_Type = EmbedBase::Unit_Type;

    using Point  = EmbedBase::Point_Type;
    using Vector = EmbedBase::Vector_Type;

    using Frame    = EmbedBase::Frame_Type;
    using ASFrame  = EmbedBase::ASFrame_Type;
    using ASFrameH = EmbedBase::ASFrameH_Type;

    using PointH  = EmbedBase::PointH_Type;
    using VectorH = EmbedBase::VectorH_Type;

    // Constructors
    SceneObject(QNode* parent = nullptr);



    // Frame access

    //! Vector space frame wrt. global coordinates
    const Frame vSpaceFrameGlobal() const;

    //! Projective space frame wrt. global coordinates
    const ASFrameH pSpaceFrameGlobal() const;

    //! Parent object's vector space frame wrt. global coordinates
    const Frame parentVSpaceFrameGlobal() const;

    //! Parent object's projective space frame wrt. global coordinates
    const ASFrameH parentPSpaceFrameGlobal() const;



    // Frame-axis access
    const Vector directionAxisGlobal() const;
    const Vector sideAxisGlobal() const;
    const Vector upAxisGlobal() const;
    const Point  frameOriginGlobal() const;

    const VectorH directionAxisGlobalH() const;
    const VectorH sideAxisGlobalH() const;
    const VectorH upAxisGlobalH() const;
    const PointH  frameOriginGlobalH() const;





    bool selected() const;
    void setSelected(bool selected_flag);
    void toggleSelected();

    Q_INVOKABLE QVector3D directionAxisGlobalQt() const;
    Q_INVOKABLE QVector3D sideAxisGlobalQt() const;
    Q_INVOKABLE QVector3D upAxisGlobalQt() const;
    Q_INVOKABLE QVector3D frameOriginGlobalQt() const;


  public slots:
    void rotateLocalQt(float angle_in_radians, const QVector3D& axis);
    void rotateParentQt(float angle_in_radians, const QVector3D& axis);
    void rotateGlobalQt(float angle_in_radians, const QVector3D& axis);
    void translateLocalQt(const QVector3D& v);
    void translateParentQt(const QVector3D& v);
    void translateGlobalQt(const QVector3D& v);
    void setFrameParentQt(const QVector3D& dir, const QVector3D& up,
                          const QVector3D& pos);
    void setFrameGlobalQt(const QVector3D& dir, const QVector3D& up,
                          const QVector3D& pos);

  public:
    void rotateLocal(double angle_in_radians, const Vector& axis) override;
    void rotateParent(double angle_in_radians, const Vector& axis) override;
    void rotateGlobal(double angle_in_radians, const Vector& axis);
    void translateLocal(const Vector& v) override;
    void translateParent(const Vector& v) override;
    void translateGlobal(const Vector& v);
    void setFrameParent(const Vector& dir, const Vector& up,
                        const Point& origin) override;
    void setFrameGlobal(const Vector& dir, const Vector& up,
                        const Point& origin);

  signals:
    void selectedChanged(bool selected) const;
    void pSpaceFrameParentChanged(const QMatrix4x4&) const;
    void pSpaceFrameGlobalChanged(const QMatrix4x4&) const;

  protected slots:
    void handleParentChanged(QObject* parent);

  private:
    bool                       m_selected_flag{false};
    Qt3DRender::QObjectPicker* m_objectpicker;

    //! Parent object's projective space frame wrt. global coordinates
    mutable ASFrameH m_parent_pspace_frame_global{
      spaces::projectivespace::identityFrame<EmbedBase::EmbedSpace>()};

    //! Projective space frame wrt. global coordinates
    mutable ASFrameH m_pspace_frame_global{
      spaces::projectivespace::identityFrame<EmbedBase::EmbedSpace>()};

    bool m_global_frames_dirty_flag{true};

    //    SceneObjectVector m_sceneobject_children;
    qint64 m_scene_depth_pos{0};

    Qt3DCore::QTransform* m_transform;

    void updateGlobalFrames(SceneObject* parent);
    void updateGlobalFrames(Scenegraph* parent);
    void endGlobalFramesUpdate();
    void endTransform();
    void markGlobalFrameDirty();
    void updateSceneDepthInfo(qint64 parent_scene_depth_pos);
    void lazyPrepare() const;

    SceneObjectVector sceneObjectChildren() const;
    Scenegraph*       getSceneGraphFromParent(QObject* parent);


  private slots:
    void pick(Qt3DRender::QPickEvent* pick);

    // Node construction
  private:
    Scenegraph*                      m_scenegraph{nullptr};
    Qt3DRender::QLayer*              m_select_layer{nullptr};
    Qt3DCore::QEntity*               m_selected_object{nullptr};
    Qt3DExtras::QPhongAlphaMaterial* m_selected_material{nullptr};

    Scenegraph* sceneGraph() const;
    void        setSceneGraph(Scenegraph* scenegraph);

    // Interface members
  public:
    virtual void initDefaultComponents() {}

  protected:
    virtual void
    initDefaultSelectionComponents(Qt3DRender::QGeometryRenderer* renderer);
  };

}   // namespace gmlib2::qt

#endif   // GM2QT_SCENEOBJECT_H
