#ifndef GM2QT_TRADITIONALGMLIBCAMERACONTROLLER_H
#define GM2QT_TRADITIONALGMLIBCAMERACONTROLLER_H

#include "../models/scenemodel.h"

// qt
#include <Qt3DRender>
#include <Qt3DInput>
#include <Qt3DCore>
#include <Qt3DLogic>

// stl
#include <memory>


namespace gmlib2::qt
{


  class TraditionalGMlibCameraController : public Qt3DCore::QEntity {
    Q_OBJECT
    Q_PROPERTY(Qt3DRender::QCamera* camera READ camera WRITE setCamera NOTIFY
                                                                       cameraChanged)
    Q_PROPERTY(SceneModel* scenemodel READ scenemodel WRITE setSceneModel NOTIFY
                                                                          scenemodelChanged)

  public:
    TraditionalGMlibCameraController(Qt3DCore::QNode* parent = nullptr);


    Qt3DRender::QCamera* camera() const;
    SceneModel*          scenemodel() const;

    void setCamera(Qt3DRender::QCamera* camera);
    void setSceneModel(SceneModel* model);

  signals:
    void cameraChanged();
    void scenemodelChanged();

    // members
  private:
    using SceneObjectVector = QVector<SceneObject*>;

    Qt3DRender::QCamera* m_camera{nullptr};
    SceneModel*          m_scenemodel{nullptr};


    Qt3DInput::QAction* m_left_mousebutton_action;
    Qt3DInput::QAction* m_right_mousebutton_action;
    Qt3DInput::QAction* m_alt_key_action;
    Qt3DInput::QAction* m_shift_key_action;
    Qt3DInput::QAction* m_ctrl_key_action;
    Qt3DInput::QAxis*   m_rx_axis_action;
    Qt3DInput::QAxis*   m_ry_axis_action;

    Qt3DInput::QActionInput* m_left_mousebutton_input;
    Qt3DInput::QActionInput* m_right_mousebutton_input;
    Qt3DInput::QActionInput* m_alt_key_input;
    Qt3DInput::QActionInput* m_shift_key_input;
    Qt3DInput::QActionInput* m_ctrl_key_input;

    Qt3DInput::QAction*      m_q_key_action;
    Qt3DInput::QActionInput* m_q_key_input;

    Qt3DInput::QAnalogAxisInput* m_mouse_rx_input;
    Qt3DInput::QAnalogAxisInput* m_mouse_ry_input;

    Qt3DInput::QKeyboardDevice* m_keyboard_device;
    Qt3DInput::QMouseDevice*    m_mouse_device;
    Qt3DInput::QLogicalDevice*  m_logical_device;

    Qt3DLogic::QFrameAction* m_frame_action;

    // helpers
    SceneObjectVector selectedSceneObjects() const;


    // handle trigger events
    void htMoveSelectedObjects(float dt) const;
    void htRotateSelectedObjects(float dt) const;



    void debug01() const;

  private slots:
    void handleTriggered(float dt);
  };

}   // namespace gmlib2::qt


#endif   // GM2QT_TRADITIONALGMLIBCAMERACONTROLLER_H
