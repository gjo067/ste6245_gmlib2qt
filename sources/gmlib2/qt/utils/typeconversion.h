#ifndef GM2QT_TYPECONVERSION_H
#define GM2QT_TYPECONVERSION_H

// gmlib2
#include <gmlib2.h>

// qt
#include <QMatrix4x4>

namespace gmlib2::qt
{

  QMatrix4x4
  toQtMatrix4x4(const typename gmlib2::ProjectiveSpaceObject<>::ASFrameH_Type& m)
  {
    // clang-format off
    return QMatrix4x4(
      float(m(0, 0)), float(m(0, 1)), float(m(0, 2)), float(m(0, 3)),
      float(m(1, 0)), float(m(1, 1)), float(m(1, 2)), float(m(1, 3)),
      float(m(2, 0)), float(m(2, 1)), float(m(2, 2)), float(m(2, 3)),
      float(m(3, 0)), float(m(3, 1)), float(m(3, 2)), float(m(3, 3)));
    // clang-format on
  }

}   // namespace gmlib2::qt


#endif   // GM2QT_TYPECONVERSION_H
